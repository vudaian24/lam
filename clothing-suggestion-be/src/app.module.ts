import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './module/user/user.module';
import { MongoModule } from './database/mongo.service';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { ProductModule } from './module/product/product.module';
import { BrandModule } from './module/brand/season.module';
import { ColorModule } from './module/color/color.module';
import { SizeModule } from './module/size/size.module';
import { CategoryModule } from './module/category/category.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true
    }),
    UserModule,
    MongoModule,
    AuthModule,
    ProductModule,
    BrandModule,
    ColorModule,
    SizeModule,
    CategoryModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
