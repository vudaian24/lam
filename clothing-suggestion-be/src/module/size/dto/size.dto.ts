export class CreateSizeDto {
    index: string;
    name?: string;
    description?: string;
}

export class UpdateSizeDto {
    index: string;
    name?: string;
    description?: string;
}

export class QuerySize {
    name?: string;
}