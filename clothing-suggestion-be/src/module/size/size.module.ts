import { Module } from '@nestjs/common';
import { SizeController } from './size.controller';
import { SizeService } from './size.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Size, SizeSchema } from '../../database/schemas/size.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Size.name, schema: SizeSchema }]),
  ],
  controllers: [SizeController],
  providers: [SizeService]
})
export class SizeModule { }
