import { HttpException, Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Size, SizeDocument } from '../../database/schemas/size.schema'; // Import Schema và Document của size
import { HttpStatus } from '../../common/constants';
import { CreateSizeDto, QuerySize, UpdateSizeDto } from './dto/size.dto'; // Import DTO của size
import { IGetListResponse } from '../../common/interfaces';

@Injectable()
export class SizeService {
    constructor(
        @InjectModel(Size.name)
        private readonly sizeModel: Model<SizeDocument>
    ) { }

    async getSizes(
        query: QuerySize = {}
    ): Promise<IGetListResponse<Size>> {
        try {
            let total: number;
            let result: Size[];
            if (query.name) {
                const regex = new RegExp(query.name, 'i');
                total = await this.sizeModel.countDocuments({ name: { $regex: regex } });
                result = await this.sizeModel.find({ name: { $regex: regex } });
            } else {
                total = await this.sizeModel.countDocuments();
                result = await this.sizeModel.find();
            }

            return {
                totalItems: total,
                items: result || [],
            };
        } catch (error) {
            throw error;
        }
    }

    async createSize(dto: CreateSizeDto) {
        try {
            const existingSize = await this.sizeModel.findOne({ name: dto.name });

            if (existingSize) {
                throw new HttpException('Size already exists', HttpStatus.BAD_REQUEST);
            }

            const size = await this.sizeModel.create(dto);
            return size;
        } catch (error) {
            throw error;
        }
    }

    async updateSize(id: Types.ObjectId, dto: UpdateSizeDto) {
        try {
            const existingSize = await this.sizeModel.findById(id);

            if (!existingSize) {
                throw new HttpException('Size not found', HttpStatus.BAD_REQUEST);
            }

            const updatedSize = await this.sizeModel.findByIdAndUpdate(id, dto, { new: true });
            return updatedSize;
        } catch (error) {
            throw error;
        }
    }

    async findSizeById(id: Types.ObjectId) {
        try {
            return await this.sizeModel.findById(id);
        } catch (error) {
            throw error;
        }
    }

    async deleteSize(id: Types.ObjectId) {
        try {
            const existingSize = await this.sizeModel.findById(id);

            if (!existingSize) {
                throw new HttpException('Size not found', HttpStatus.BAD_REQUEST);
            }

            const deleted = await this.sizeModel.findByIdAndDelete(id);
            return deleted;
        } catch (error) {
            throw error;
        }
    }
}
