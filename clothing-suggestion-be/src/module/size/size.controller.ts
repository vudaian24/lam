import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { BaseController } from '../../common/base/base.controller';
import { SizeService } from './size.service'; // Import service của size
import { SuccessResponse } from '../../common/response';
import { CreateSizeDto, UpdateSizeDto, QuerySize } from './dto/size.dto'; // Import DTO tương ứng
import { toObjectId } from '../../common/commonFunctions';

@Controller('size')
export class SizeController extends BaseController {
    constructor(private readonly sizeService: SizeService) {
        super();
    }

    @Get()
    async findAll(
        @Query() query: QuerySize
    ) {
        try {
            const sizes = await this.sizeService.getSizes(query);
            return new SuccessResponse(sizes);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post()
    async createSize(
        @Body() dto: CreateSizeDto,
    ) {
        try {
            const result = await this.sizeService.createSize(dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Patch(':id')
    async updateSize(
        @Param('id') id: string,
        @Body() dto: UpdateSizeDto,
    ) {
        try {
            const result = await this.sizeService.updateSize(toObjectId(id), dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get(':id')
    async getSizeDetail(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.sizeService.findSizeById(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Delete(':id')
    async deleteSize(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.sizeService.deleteSize(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
}
