import { HttpException, Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Category, CategoryDocument } from '../../database/schemas/type.schema';
import { HttpStatus } from '../../common/constants';
import { CreateCategoryDto, QueryCategory, UpdateCategoryDto } from './dto/category.dto';
import { IGetListResponse } from '../../common/interfaces';

@Injectable()
export class CategoryService {
    constructor(
        @InjectModel(Category.name)
        private readonly categoryModel: Model<CategoryDocument>
    ) { }

    async getCategories(
        query: QueryCategory = {}
    ): Promise<IGetListResponse<Category>> {
        try {
            let total: number;
            let resutl: Category[];
            if (query.name) {
                const regex = new RegExp(query.name, 'i');
                total = await this.categoryModel.countDocuments({ name: { $regex: regex } });
                resutl = await this.categoryModel.find({ name: { $regex: regex } });
            } else {
                total = await this.categoryModel.countDocuments();
                resutl = await this.categoryModel.find();
            }

            return {
                totalItems: total,
                items: resutl || [],
            };
        } catch (error) {
            throw error;
        }
    }

    async createCategory(dto: CreateCategoryDto) {
        try {
            const existingCategory = await this.categoryModel.findOne({ name: dto.name });

            if (existingCategory) {
                throw new HttpException('Category already exists', HttpStatus.BAD_REQUEST);
            }

            const category = await this.categoryModel.create(dto);
            return category;
        } catch (error) {
            throw error;
        }
    }

    async updateCategory(id: Types.ObjectId, dto: UpdateCategoryDto) {
        try {
            const existingCategory = await this.categoryModel.findById(id);

            if (!existingCategory) {
                throw new HttpException('Category not found', HttpStatus.BAD_REQUEST);
            }

            const updatedCategory = await this.categoryModel.findByIdAndUpdate(id, dto, { new: true });
            return updatedCategory;
        } catch (error) {
            throw error;
        }
    }

    async findCategoryById(id: Types.ObjectId) {
        try {
            return await this.categoryModel.findById(id);
        } catch (error) {
            throw error;
        }
    }

    async deleteCategory(id: Types.ObjectId) {
        try {
            const existingCategory = await this.categoryModel.findById(id);

            if (!existingCategory) {
                throw new HttpException('Category not found', HttpStatus.BAD_REQUEST);
            }

            const deleted = await this.categoryModel.findByIdAndDelete(id);
            return deleted;
        } catch (error) {
            throw error;
        }
    }
}
