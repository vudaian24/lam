export class CreateCategoryDto {
    index: string;
    name?: string;
    description?: string;
}

export class UpdateCategoryDto {
    index: string;
    name?: string;
    description?: string;
}

export class QueryCategory {
    name?: string;
}