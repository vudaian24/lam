import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { BaseController } from '../../common/base/base.controller';
import { ColorService } from './color.service'; // Import service của color
import { SuccessResponse } from '../../common/response';
import { CreateColorDto, UpdateColorDto, QueryColor } from './dto/color.dto'; // Import DTO tương ứng
import { toObjectId } from '../../common/commonFunctions';

@Controller('color')
export class ColorController extends BaseController {
    constructor(private readonly colorService: ColorService) {
        super();
    }

    @Get()
    async findAll(
        @Query() query: QueryColor
    ) {
        try {
            const colors = await this.colorService.getColors(query);
            return new SuccessResponse(colors);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post()
    async createColor(
        @Body() dto: CreateColorDto,
    ) {
        try {
            const result = await this.colorService.createColor(dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Patch(':id')
    async updateColor(
        @Param('id') id: string,
        @Body() dto: UpdateColorDto,
    ) {
        try {
            const result = await this.colorService.updateColor(toObjectId(id), dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get(':id')
    async getColorDetail(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.colorService.findColorById(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Delete(':id')
    async deleteColor(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.colorService.deleteColor(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
}
