export class CreateColorDto {
    index: string;
    name?: string;
    description?: string;
}

export class UpdateColorDto {
    index: string;
    name?: string;
    description?: string;
}

export class QueryColor {
    name?: string;
}