import { HttpException, Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Color, ColorDocument } from '../../database/schemas/color.schema'; // Import Schema và Document của color
import { HttpStatus } from '../../common/constants';
import { CreateColorDto, QueryColor, UpdateColorDto } from './dto/color.dto'; // Import DTO của color
import { IGetListResponse } from '../../common/interfaces';

@Injectable()
export class ColorService {
    constructor(
        @InjectModel(Color.name)
        private readonly colorModel: Model<ColorDocument>
    ) { }

    async getColors(
        query: QueryColor = {}
    ): Promise<IGetListResponse<Color>> {
        try {
            let total: number;
            let result: Color[];
            if (query.name) {
                const regex = new RegExp(query.name, 'i');
                total = await this.colorModel.countDocuments({ name: { $regex: regex } });
                result = await this.colorModel.find({ name: { $regex: regex } });
            } else {
                total = await this.colorModel.countDocuments();
                result = await this.colorModel.find();
            }

            return {
                totalItems: total,
                items: result || [],
            };
        } catch (error) {
            throw error;
        }
    }

    async createColor(dto: CreateColorDto) {
        try {
            const existingColor = await this.colorModel.findOne({ name: dto.name });

            if (existingColor) {
                throw new HttpException('Color already exists', HttpStatus.BAD_REQUEST);
            }

            const color = await this.colorModel.create(dto);
            return color;
        } catch (error) {
            throw error;
        }
    }

    async updateColor(id: Types.ObjectId, dto: UpdateColorDto) {
        try {
            const existingColor = await this.colorModel.findById(id);

            if (!existingColor) {
                throw new HttpException('Color not found', HttpStatus.BAD_REQUEST);
            }

            const updatedColor = await this.colorModel.findByIdAndUpdate(id, dto, { new: true });
            return updatedColor;
        } catch (error) {
            throw error;
        }
    }

    async findColorById(id: Types.ObjectId) {
        try {
            return await this.colorModel.findById(id);
        } catch (error) {
            throw error;
        }
    }

    async deleteColor(id: Types.ObjectId) {
        try {
            const existingColor = await this.colorModel.findById(id);

            if (!existingColor) {
                throw new HttpException('Color not found', HttpStatus.BAD_REQUEST);
            }

            const deleted = await this.colorModel.findByIdAndDelete(id);
            return deleted;
        } catch (error) {
            throw error;
        }
    }
}
