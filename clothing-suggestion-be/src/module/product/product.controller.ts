import { BaseController } from '../../common/base/base.controller';
import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { ProductService } from './product.service';
import { SuccessResponse } from '../../common/response';
import { CreateProductDto, QueryProduct, UpdateProductDto } from './dto/product.dto';
import { toObjectId } from '../../common/commonFunctions';

@Controller('product')
export class ProductController extends BaseController {
    constructor(private readonly productService: ProductService) {
        super();
    }
    @Get()
    async findAll(
        @Query() query: QueryProduct
    ) {
        try {
            const products = await this.productService.getProducts(query);
            return new SuccessResponse(products);
        }
        catch (error) {
            this.handleError(error);
        }
    }
    @Post()
    async createProduct(
        @Body() dto: CreateProductDto,
    ) {
        try {
            const result = await this.productService.createProduct(dto);
            return new SuccessResponse(result);
        }
        catch (error) {
            this.handleError(error);
        }
    }
    @Patch(':id')
    async updateProduct(
        @Param('id')
        id: string,
        @Body() dto: UpdateProductDto,
    ) {
        try {
            const result = await this.productService.updateProduct(toObjectId(id), dto);
            return new SuccessResponse(result);
        }
        catch (error) {
            this.handleError(error);
        }
    }
    @Delete(':id')
    async deleteProduct(
        @Param('id')
        id: string,
    ) {
        try {
            const result = await this.productService.deleteProduct(toObjectId(id));
            return new SuccessResponse(result);
        }
        catch (error) {
            this.handleError(error);
        }
    }
    @Get(':id')
    async getImage(
        @Param('id')
        id: string,
    ) {
        try {
            const result = await this.productService.findById(toObjectId(id));
            return new SuccessResponse(result);
        }
        catch (error) {
            this.handleError(error);
        }
    }

    @Post('category')
    async getProductByCategory(
        @Body() query: {query: string}
    ) {
        try {
            const result = await this.productService.getProductByCategory(query.query);
            return new SuccessResponse(result);
        }
        catch (error) {
            this.handleError(error);
        }
    }
}
