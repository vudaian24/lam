export class CreateProductDto {
    name?: string;
    description?: string;
    price?: number;
    quantity?: number;
    image?: string;
    category?: string;
    color?: string;
    brand?: string;
    size?: string;
    age?:string;
}

export class UpdateProductDto {
    name?: string;
    description?: string;
    price?: number;
    quantity?: number;
    image?: string;
    category?: string;
    color?: string;
    brand?: string;
    size?: string;
    age?:string;
}

export class QueryProduct {
    name?: string;
}