import { IGetListResponse } from '../../common/interfaces';
import { Product } from '../../database/schemas/product.schema';
import { Body, HttpException, Injectable, Post, Query } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateProductDto, QueryProduct, UpdateProductDto } from './dto/product.dto';
import { HttpStatus } from '../../common/constants';
import { ProductAttributesForDetail, ProductAttributesForList } from '../constants';

@Injectable()
export class ProductService {
    constructor(
        @InjectModel(Product.name)
        private readonly productModel: Model<Product>,
    ) { }
    async getProducts(
        query: QueryProduct = {}
    ): Promise<IGetListResponse<Product>> {
        try {
            let total: number;
            let products: Product[];

            if (query.name) {
                const regex = new RegExp(query.name, 'i');
                total = await this.productModel.countDocuments({ name: { $regex: regex } });
                products = await this.productModel.find({ name: { $regex: regex } });
            } else {
                total = await this.productModel.countDocuments();
                products = await this.productModel.find();
            }

            return {
                totalItems: total,
                items: products,
            };
        } catch (error) {
            return {
                totalItems: 0,
                items: [],
            };
        }
    }

    async getProductByCategory(query: string): Promise<IGetListResponse<Product>> {
        try {
            let total: number;
            let products: Product[];
            if (query) {
                const regex = new RegExp(query, 'i');
                total = await this.productModel.countDocuments({ brand: { $regex: regex } });
                products = await this.productModel.find({ brand: { $regex: regex } });
            } else {
                total = await this.productModel.countDocuments();
                products = await this.productModel.find();
            }
            return {
                totalItems: total,
                items: products,
            };
        } catch (error) {
            return {
                totalItems: 0,
                items: [],
            };
        }
    }


    async createProduct(dto: CreateProductDto) {
        try {
            const newProduct = new this.productModel(dto);
            return newProduct.save();
        }
        catch (error) {
            throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async updateProduct(id: Types.ObjectId, dto: UpdateProductDto) {
        try {
            const existing = await this.productModel.findOne({
                _id: id,
                deletedAt: null,
            });
            if (!existing) {
                throw new HttpException('Product not found', HttpStatus.BAD_REQUEST);
            }
            const updated = await this.productModel.findByIdAndUpdate(id, dto, { new: true });
            return updated;
        } catch (err) {
            throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async findById(
        id: Types.ObjectId,
        attributes: (keyof Product)[] = ProductAttributesForDetail,
    ) {
        try {
            return await this.productModel.findById(id, attributes);
        } catch (error) {
            throw error;
        }
    }
    async deleteProduct(id: Types.ObjectId) {
        try {
            const existing = await this.productModel.findOne({
                _id: id,
                deletedAt: null,
            });
            if (!existing) {
                throw new HttpException('Product not found', HttpStatus.BAD_REQUEST);
            }
            const deleted = await this.productModel.deleteOne(id);
            return deleted;
        } catch (err) {
            throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
