import { Category } from '../database/schemas/type.schema';
import { Season } from '../database/schemas/season.schema';
import { Color } from '../database/schemas/color.schema';
import { Size } from '../database/schemas/size.schema';
import { User } from '../database/schemas/user.schema';
import { Product } from '../database/schemas/product.schema';

// Constants cho User
export enum UserOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
}

export const UserAttributesForList: (keyof User)[] = [
    '_id',
    'email',
    'username',
    'role',
    'createdAt',
];

export const UserAttributesForDetail: (keyof User)[] = ['_id', 'email', 'username', 'role'];

// Constants cho Product
export enum ProductOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
}

export const ProductAttributesForList: (keyof Product)[] = [
    '_id',
    'name',
    'description',
    'image',
    'category',
    'color',
    'brand',
    'size',
    'createdAt',
];

export const ProductAttributesForDetail: (keyof Product)[] = [
    '_id',
    'name',
    'description',
    'price',
    'quantity',
    'image',
    'category',
    'color',
    'brand',
    'size',
    'createdAt',
];

// Constants cho Category
export enum CategoryOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
}

export const CategoryAttributesForList: (keyof Category)[] = [
    '_id',
    'name',
    'description',
    'createdAt',
];

export const CategoryAttributesForDetail: (keyof Category)[] = ['_id', 'name', 'description'];

// Constants cho Brand
export enum BrandOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
}

export const BrandAttributesForList: (keyof Season)[] = [
    '_id',
    'name',
    'description',
    'createdAt',
];

export const BrandAttributesForDetail: (keyof Season)[] = ['_id', 'name', 'description'];

// Constants cho Color
export enum ColorOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
}

export const ColorAttributesForList: (keyof Color)[] = [
    '_id',
    'name',
    'description',
    'createdAt',
];

export const ColorAttributesForDetail: (keyof Color)[] = ['_id', 'name', 'description'];

// Constants cho Size
export enum SizeOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
}

export const SizeAttributesForList: (keyof Size)[] = [
    '_id',
    'name',
    'description',
    'createdAt',
];

export const SizeAttributesForDetail: (keyof Size)[] = ['_id', 'name', 'description'];
