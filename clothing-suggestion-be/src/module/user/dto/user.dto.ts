export class CreateUserDto {
    email?: string;
    username?: string;
    password?: string;
    role: string;
}

export class UpdateUserDto {
    email: string;
    username: string;
    password: string;
    role: string;
}

export class QueryUser {
    username?: string;
}