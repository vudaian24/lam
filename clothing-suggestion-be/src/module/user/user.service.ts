import { HttpException, Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../../database/schemas/user.schema';
import { HttpStatus } from '../../common/constants';
import { UserAttributesForDetail, UserAttributesForList } from '../constants';
import { CreateUserDto, QueryUser, UpdateUserDto } from './dto/user.dto';
import { IGetListResponse } from '../../common/interfaces';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
    constructor(
        @InjectModel(User.name)
        private readonly userModel: Model<UserDocument>
    ) { }

    async getUsers(
        query: QueryUser = {}
    ): Promise<IGetListResponse<User>> {
        try {
            let total: number;
            let resutl: User[];
            if (query.username) {
                const regex = new RegExp(query.username, 'i');
                total = await this.userModel.countDocuments({ username: { $regex: regex } });
                resutl = await this.userModel.find({ username: { $regex: regex } });
            } else {
                total = await this.userModel.countDocuments();
                resutl = await this.userModel.find();
            }

            return {
                totalItems: total,
                items: resutl || [],
            };
        } catch (error) {
            throw error;
        }
    }

    async createUser(dto: CreateUserDto) {
        try {
            const existingUser = await this.userModel.findOne({
                username: dto.username,
                deletedAt: null,
            });

            if (existingUser) {
                throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
            }

            const hashedPassword: string = await bcrypt.hash(dto.password, 10);

            const user: SchemaCreateDocument<User> = {
                ...(dto as any),
                password: hashedPassword,
            };

            const createUser = this.userModel.create(user);
            return createUser;
        } catch (error) {
            throw error;
        }
    }

    async updateUser(id: Types.ObjectId, dto: UpdateUserDto) {
        try {
            const existingUser = await this.userModel.findOne({
                _id: id,
                deletedAt: null,
            });

            if (!existingUser) {
                throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
            }

            const updateUser = await this.userModel.findByIdAndUpdate(id, dto, { new: true });

            return updateUser;
        } catch (error) {
            throw error;
        }
    }

    async findUserById(
        id: Types.ObjectId,
        attributes: (keyof User)[] = UserAttributesForDetail,
    ) {
        try {
            return await this.userModel.findById(id, attributes);
        } catch (error) {
            throw error;
        }
    }

    async deleteUser(id: Types.ObjectId) {
        try {
            const existingUser = await this.userModel.findOne({
                _id: id,
                deletedAt: null,
            });
            if (!existingUser) {
                throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
            }
            const deleted = await this.userModel.deleteOne(id);
            return deleted;
        } catch (error) {
            throw error;
        }
    }
}
