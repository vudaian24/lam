import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { BaseController } from '../../common/base/base.controller';
import { UserService } from './user.service';
import { SuccessResponse } from '../../common/response';
import { CreateUserDto, QueryUser, UpdateUserDto } from './dto/user.dto';
import { toObjectId } from '../../common/commonFunctions';

@Controller('user')
export class UserController extends BaseController {
    constructor(private readonly userService: UserService) {
        super();
    }

    @Get()
    async findAll(
        @Query() query: QueryUser
    ) {
        try {
            const users = await this.userService.getUsers(query);
            return new SuccessResponse(users);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post()
    async createUser(
        @Body() dto: CreateUserDto,
    ) {
        try {
            const result = await this.userService.createUser(dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Patch(':id')
    async updateUser(
        @Param('id')
        id: string,
        @Body()
        dto: UpdateUserDto,
    ) {
        try {
            const result = await this.userService.updateUser(toObjectId(id), dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get(':id')
    async getUserDetail(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.userService.findUserById(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Delete(':id')
    async deleteUser(
        @Param('id')
        id: string,
    ) {
        try {
            const result = await this.userService.deleteUser(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
}
