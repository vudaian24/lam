export class CreateBrandDto {
    index: string;
    name?: string;
    description?: string;
}

export class UpdateBrandDto {
    index: string;
    name?: string;
    description?: string;
}

export class QueryBrand {
    name?: string;
}