import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { BaseController } from '../../common/base/base.controller';
import { BrandService } from './season.service'; // Import service của brand
import { SuccessResponse } from '../../common/response';
import { CreateBrandDto, UpdateBrandDto, QueryBrand } from './dto/season.dto'; // Import DTO tương ứng
import { toObjectId } from '../../common/commonFunctions';

@Controller('brand')
export class BrandController extends BaseController {
    constructor(private readonly brandService: BrandService) {
        super();
    }

    @Get()
    async findAll(
        @Query() query: QueryBrand
    ) {
        try {
            const brands = await this.brandService.getBrands(query);
            return new SuccessResponse(brands);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get('all')
    async getAll() {
        try {
            const brands = await this.brandService.getAllBrands();
            return new SuccessResponse(brands);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post()
    async createBrand(
        @Body() dto: CreateBrandDto,
    ) {
        try {
            const result = await this.brandService.createBrand(dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Patch(':id')
    async updateBrand(
        @Param('id') id: string,
        @Body() dto: UpdateBrandDto,
    ) {
        try {
            const result = await this.brandService.updateBrand(toObjectId(id), dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get(':id')
    async getBrandDetail(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.brandService.findBrandById(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Delete(':id')
    async deleteBrand(
        @Param('id') id: string,
    ) {
        try {
            const result = await this.brandService.deleteBrand(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
}
