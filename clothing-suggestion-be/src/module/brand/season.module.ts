import { Module } from '@nestjs/common';
import { BrandController } from './season.controller';
import { BrandService } from './season.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Season, SeasonSchema } from '../../database/schemas/season.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Season.name, schema: SeasonSchema }]),
  ],
  controllers: [BrandController],
  providers: [BrandService]
})
export class BrandModule { }
