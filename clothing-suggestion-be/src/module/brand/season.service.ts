import { HttpException, Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Season, SeasonDocument } from '../../database/schemas/season.schema'; // Import Schema và Document của thương hiệu
import { HttpStatus } from '../../common/constants';
import { CreateBrandDto, QueryBrand, UpdateBrandDto } from './dto/season.dto'; // Import DTO của thương hiệu
import { IGetListResponse } from '../../common/interfaces';

@Injectable()
export class BrandService {
    constructor(
        @InjectModel(Season.name)
        private readonly brandModel: Model<SeasonDocument>
    ) { }

    async getBrands(
        query: QueryBrand = {}
    ): Promise<IGetListResponse<Season>> {
        try {
            let total: number;
            let result: Season[];
            if (query.name) {
                const regex = new RegExp(query.name, 'i');
                total = await this.brandModel.countDocuments({ name: { $regex: regex } });
                result = await this.brandModel.find({ name: { $regex: regex } });
            } else {
                total = await this.brandModel.countDocuments();
                result = await this.brandModel.find();
            }

            return {
                totalItems: total,
                items: result || [],
            };
        } catch (error) {
            throw error;
        }
    }

    async getAllBrands(): Promise<IGetListResponse<Season>> {
        try {
            const total = await this.brandModel.countDocuments();
            const result = await this.brandModel.find();
            return {
                totalItems: total,
                items: result || [],
            };
        } catch (error) {
            throw error;
        }
    }

    async createBrand(dto: CreateBrandDto) {
        try {
            const existingBrand = await this.brandModel.findOne({ name: dto.name });

            if (existingBrand) {
                throw new HttpException('Brand already exists', HttpStatus.BAD_REQUEST);
            }

            const brand = await this.brandModel.create(dto);
            return brand;
        } catch (error) {
            throw error;
        }
    }

    async updateBrand(id: Types.ObjectId, dto: UpdateBrandDto) {
        try {
            const existingBrand = await this.brandModel.findById(id);

            if (!existingBrand) {
                throw new HttpException('Brand not found', HttpStatus.BAD_REQUEST);
            }

            const updatedBrand = await this.brandModel.findByIdAndUpdate(id, dto, { new: true });
            return updatedBrand;
        } catch (error) {
            throw error;
        }
    }

    async findBrandById(id: Types.ObjectId) {
        try {
            return await this.brandModel.findById(id);
        } catch (error) {
            throw error;
        }
    }

    async deleteBrand(id: Types.ObjectId) {
        try {
            const existingBrand = await this.brandModel.findById(id);

            if (!existingBrand) {
                throw new HttpException('Brand not found', HttpStatus.BAD_REQUEST);
            }

            const deleted = await this.brandModel.findByIdAndDelete(id);
            return deleted;
        } catch (error) {
            throw error;
        }
    }
}
