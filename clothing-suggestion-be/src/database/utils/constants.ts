export enum MongoCollection {
    USERS = 'users',
    PRODUCT = 'products',
    CATEGORY = 'category',
    SEASON = 'season',
    COLOR = 'color',
    SIZE ='size',
}
