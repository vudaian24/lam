import { Prop, Schema } from '@nestjs/mongoose';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
import { MongoBaseSchema } from '../../common/base/base.schema';

export type ProductDocument = SchemaDocument<Product>;
@Schema({
    timestamps: true,
    collection: MongoCollection.PRODUCT,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Product extends MongoBaseSchema {
    @Prop({ required: false })
    name: string;
    @Prop({ required: false })
    description: string;
    @Prop({ required: false })
    price: number;
    @Prop({ required: false })
    quantity: number;
    @Prop({ required: false })
    image: string;
    @Prop({ required: false })
    category: string;
    @Prop({ required: false })
    color: string;
    @Prop({ required: false })
    brand: string;
    @Prop({ required: false })
    size: string;
}
const ProductSchema = createSchemaForClass(Product);

export { ProductSchema };