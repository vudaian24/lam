import { Prop, Schema } from '@nestjs/mongoose';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
import { MongoBaseSchema } from '../../common/base/base.schema';

export type SizeDocument = SchemaDocument<Size>;
@Schema({
    timestamps: true,
    collection: MongoCollection.SIZE,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Size extends MongoBaseSchema {
    @Prop({ required: false })
    index: string;
    @Prop({ required: false })
    name: string;
    @Prop({ required: false })
    description: string;
}
const SizeSchema = createSchemaForClass(Size);

export { SizeSchema };