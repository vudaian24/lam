import { Prop, Schema } from '@nestjs/mongoose';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
import { MongoBaseSchema } from '../../common/base/base.schema';

export type SeasonDocument = SchemaDocument<Season>;
@Schema({
    timestamps: true,
    collection: MongoCollection.SEASON,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Season extends MongoBaseSchema {
    @Prop({ required: false })
    index: string;
    @Prop({ required: false })
    name: string;
    @Prop({ required: false })
    description: string;
}
const SeasonSchema = createSchemaForClass(Season);

export { SeasonSchema };