import { Prop, Schema } from '@nestjs/mongoose';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
import { MongoBaseSchema } from '../../common/base/base.schema';

export type ColorDocument = SchemaDocument<Color>;
@Schema({
    timestamps: true,
    collection: MongoCollection.COLOR,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Color extends MongoBaseSchema {
    @Prop({ required: false })
    index: string;
    @Prop({ required: false })
    name: string;
    @Prop({ required: false })
    description: string;
}
const ColorSchema = createSchemaForClass(Color);

export { ColorSchema };