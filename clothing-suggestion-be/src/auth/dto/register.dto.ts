export class RegisterDto {
    username: string;
    password: string;
    email: string;
    avatar:string;
    role: string;
}