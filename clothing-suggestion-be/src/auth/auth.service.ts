import { User, UserDocument } from '../database/schemas/user.schema';
import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { LoginDto } from './dto/login.dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { RegisterDto } from './dto/register.dto';
import { Request } from 'express';
import { HttpStatus } from '../common/constants';

@Injectable()
export class AuthService {
    constructor(
        @InjectModel(User.name)
        private readonly userModel: Model<UserDocument>,
        private readonly jwtService: JwtService,
        private configService: ConfigService,
    ) { }
    async login(loginDto: LoginDto): Promise<any> {
        try {
            const user = await this.userModel.findOne({ email: loginDto.email });
            if (!user) {
                throw new HttpException('User not found', HttpStatus.UNAUTHORIZED);
            }
            const isMatch = await this.comparePassword(
                user,
                loginDto.password
            );
            if (!isMatch) {
                throw new HttpException('Invalid password', HttpStatus.UNAUTHORIZED);
            }
            const payload = { id: user.id, email: user.email };
            return this.generateToken(payload);
        } catch (error) {
            throw error;
        }
    }

    async register(registerDto: RegisterDto): Promise<any> {
        try {
            const existingUser = await this.userModel.findOne({ email: registerDto.email });
            if (existingUser) {
                throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
            }
            const hashedPassword = await bcrypt.hash(registerDto.password, 10);
            const user: SchemaCreateDocument<User> = {
                ...(registerDto as any),
                password: hashedPassword,
            };
            const createUser = this.userModel.create(user);
            return createUser;
        } catch (error) {
            throw error;
        }
    }

    async getUser(request: Request): Promise<any> {
        try {
            const accessToken = this.extractTokenFromHeader(request);
            const verify = await this.jwtService.verifyAsync(accessToken, {
                secret: 'tabichlam'
            });
            const user = await this.userModel.findOne({
                email: verify.email,
            });

            if (!user) {
                throw new HttpException('User not found', HttpStatus.NOT_FOUND);
            }
            
            return {
                id: user.id,
                email: user.email,
                username: user.username,
                avatar: user.avatar,
                role: user.role,
            };
        } catch (error) {
            if (error instanceof HttpException) {
                throw error;
            }
            if (error.name === 'TokenExpiredError') {
                throw new HttpException('Refresh token has expired', HttpStatus.UNAUTHORIZED);
            } else if (error.name === 'JsonWebTokenError') {
                throw new HttpException('Invalid refresh token', HttpStatus.UNAUTHORIZED);
            } else {
                throw new HttpException('Error verifying refresh token', HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
    private extractTokenFromHeader(request: Request): string | undefined {
        const [type, token] = request.headers.authorization ? request.headers.authorization.split(' ') : [];
        return type === 'Bearer' ? token : undefined;
    }

    private async generateToken(payload: { id: string; email: string }): Promise<any> {
        const expiresIn = 604800;
        const accessToken = await this.jwtService.signAsync(payload, {
            secret: 'tabichlam',
            expiresIn: "7d",
        });
        return { accessToken, expiresIn };
    }

    private convertTimeToSeconds(expTime: string): number {
        const numericValue = parseInt(expTime);
        const unit = expTime.slice(-1);

        let seconds = 0;
        switch (unit) {
            case 's':
                seconds = numericValue;
                break;
            case 'm':
                seconds = numericValue * 60;
                break;
            case 'h':
                seconds = numericValue * 60 * 60;
                break;
            case 'd':
                seconds = numericValue * 24 * 60 * 60;
                break;
            default:
                throw new Error('Invalid time unit');
        }
        return seconds;
    }

    async comparePassword(user, password): Promise<boolean> {
        const isMatch = await bcrypt.compare(password, user.password);
        // const isMatch = password === user.password;
        return isMatch;
    }
}
