import { Prop } from '@nestjs/mongoose';
import { SchemaTypes, Types } from 'mongoose';

export class MongoBaseSchema {
    _id: Types.ObjectId;

    @Prop({ required: false, default: null, type: Number })
    createdAt: number;

    // Convert Date to timestamp
    toTimestamp(date: Date | number): number {
        if (date instanceof Date) {
            return date.getTime();
        }
        return date;
    }

    toTimestamps(): void {
        this.createdAt = this.toTimestamp(this.createdAt);
    }
}