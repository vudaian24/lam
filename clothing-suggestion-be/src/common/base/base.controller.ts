import {
    HttpException,
    Inject,
    InternalServerErrorException,
} from '@nestjs/common';

export class BaseController {
    @Inject()

    handleError(err: any): void {
        if (err && err instanceof HttpException) {
            throw err;
        }
        throw new InternalServerErrorException(err);
    }
}
