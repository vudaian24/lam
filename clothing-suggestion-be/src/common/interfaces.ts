export interface IGetListResponse<T = any> {
    totalItems: number;
    items: T[];
}
