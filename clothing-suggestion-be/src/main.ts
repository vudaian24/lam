import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import winston, { transports } from 'winston';
import { WinstonModule } from 'nest-winston';
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
import { ConfigService } from '@nestjs/config';
import helmet from 'helmet';

enum ConfigKey {
  NODE_ENV = 'NODE_ENV',
  PORT = 'PORT',
  VERSION = 'VERSION',
  BASE_PATH = 'BASE_PATH',
  CORS_WHITELIST = 'CORS_WHITELIST',
  TIMEZONE_DEFAULT_NAME = 'TIMEZONE_DEFAULT_NAME',
  MONGO_DATABASE_CONNECTION_STRING = 'MONGO_DATABASE_CONNECTION_STRING',
}
async function bootstrap() {
  const isLocal = process.env.NODE_ENV === 'local';
  const app = await NestFactory.create(AppModule);
  app.use(helmet());
  const configService = app.get(ConfigService);
  const whiteList = configService.get(ConfigKey.CORS_WHITELIST) || '*';
  const corsOptions: CorsOptions = {
    origin:
      whiteList?.split(',')?.length > 1
        ? whiteList.split(',')
        : whiteList,
    // origin: true,
    allowedHeaders: [
      'Content-Type',
      'Authorization',
      'Language',
      'X-Timezone',
      'X-Timezone-Name',
      'X-Mssp-Id',
      'X-Organization-Id',
    ],
    optionsSuccessStatus: 200,
    methods: ['GET', 'PUT', 'POST', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
  };

  app.enableCors(corsOptions);
  app.setGlobalPrefix(configService.get(ConfigKey.BASE_PATH));

  await app.listen(configService.get(ConfigKey.PORT));
}
bootstrap();
