import pandas as pd
from sklearn.svm import SVC

# Đọc dữ liệu huấn luyện từ file CSV
train_data_path = 'C:\\Workspace\\Lâm\\fastAPI\\dataset.csv'
train_df = pd.read_csv(train_data_path)

# Chọn các trường đặc trưng (features) và nhãn (label)
features = ['Material', 'Color', 'Category', 'Length', 'Season']
label = 'Style'

# Loại bỏ các hàng có giá trị NaN trong cột nhãn của tập huấn luyện
train_df = train_df.dropna(subset=[label])

# Tiền xử lý dữ liệu (mã hóa các biến phân loại)
train_df = pd.get_dummies(train_df, columns=features)

# Tách các trường đặc trưng và nhãn
X_train = train_df.drop(columns=[label, 'Image'])
y_train = train_df[label]

# Tạo và huấn luyện mô hình SVM
model = SVC(probability=True)
model.fit(X_train, y_train)

# Nhập dữ liệu mới từ người dùng
new_data = {
    'Material': 16,
    'Color': 38,
    'Type': 2,
    'Length': 10,
    'Season': 2
}

# Chuyển đổi dữ liệu mới thành DataFrame và mã hóa
new_df = pd.DataFrame([new_data])
new_df = pd.get_dummies(new_df)
new_df = new_df.reindex(columns=X_train.columns, fill_value=0)

# Dự đoán nhãn cho dữ liệu mới
new_pred = model.predict(new_df)
print(f'Dự đoán nhãn cho dữ liệu mới: {new_pred[0]}')

# Tìm các dữ liệu tương đồng trong tập dữ liệu ban đầu
similar_items = train_df[train_df[label] == new_pred[0]]

# Xuất tất cả các dữ liệu tương đồng với đầy đủ các cột
print(f'Các dữ liệu tương đồng với nhãn {new_pred[0]}:')
similar_items = similar_items.drop(columns=[col for col in similar_items.columns if '_' in col])
print(similar_items)
