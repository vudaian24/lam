from fastapi import APIRouter, HTTPException
from typing import List, Dict
from fastapi.responses import JSONResponse
import json
from pydantic import BaseModel
import pandas as pd
from sklearn.svm import SVC

router = APIRouter()

# Route check (GET)
@router.get("/check")
async def check():
    return {"message": "Check successful"}


class PredictionRequest(BaseModel):
    Material: int
    Color: int
    Category: int
    Size: int
    Season: int

@router.post("/items/")
async def create_item(request: PredictionRequest):
    # Load training data from CSV file
    train_data_path = 'C:\\Workspace\\Lâm\\fastAPI\\dataset.csv'
    train_df = pd.read_csv(train_data_path)

    # Select features and label
    features = ['Material', 'Color', 'Category', 'Length', 'Season']
    label = 'Style'

    # Drop rows with NaN values in label column of training set
    train_df = train_df.dropna(subset=[label])

    # Preprocess data (encode categorical variables)
    train_df = pd.get_dummies(train_df, columns=features)

    # Split features and label
    X_train = train_df.drop(columns=[label, 'Image'])
    y_train = train_df[label]

    # Create and train SVM model
    model = SVC(probability=True)
    model.fit(X_train, y_train)

    new_data = {
        'Material': request.Material,
        'Color': request.Color,
        'Category': request.Category,
        'Length': request.Size,
        'Season': request.Season
    }

    new_df = pd.DataFrame([new_data])
    new_df = pd.get_dummies(new_df)
    new_df = new_df.reindex(columns=X_train.columns, fill_value=0)

    new_pred = model.predict(new_df)
    similar_items = train_df[train_df[label] == new_pred[0]]
    similar_items = similar_items.drop(columns=[col for col in similar_items.columns if '_' in col])

    similar_items_data = similar_items.to_dict(orient='records')

    # Prepare the response
    response_data = []
    for item in similar_items_data:
        response_data.append({
            'Style': item['Style'],
            'Image': item['Image']
        })

    return response_data


class PredictionSearch(BaseModel):
    Category: str

@router.post("/all/")
async def get_all_items(request: PredictionSearch):
    try:
        # Load training data from CSV file
        train_data_path = 'C:\\Workspace\\Lâm\\fastAPI\\dataset.csv'
        train_df = pd.read_csv(train_data_path)

        # Drop rows with NaN values in label column of training set
        train_df = train_df.dropna(subset=['Style'])

        # If Category is provided and not an empty string, filter data by Category
        if request.Category.strip():  # Check if Category is not an empty string
            try:
                category_value = int(request.Category)
                train_df = train_df[train_df['Category'] == category_value]
            except ValueError:
                raise HTTPException(status_code=400, detail="Category must be a valid integer")

        # Select only required columns
        selected_columns = ['Image', 'Color', 'Category']
        train_df = train_df[selected_columns]

        # Prepare the response data
        response_data = train_df.to_dict(orient='records')

        return response_data
    except Exception as e:
        # Handle exceptions
        raise HTTPException(status_code=500, detail=str(e))
