import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.tree import plot_tree
# Đọc dữ liệu từ tệp CSV
df = pd.read_csv('C:\\Workspace\\Lâm\\Chatbox\\dataset.csv')

# Chọn các đặc trưng (features) và nhãn (target)
X = df.drop('Type', axis=1)  # Loại bỏ cột Type, đây là nhãn
y = df['Type']

# Chuyển đổi các biến phân loại thành các biến số bằng phương pháp One-Hot Encoding
X_encoded = pd.get_dummies(X)

# Chia dữ liệu thành tập huấn luyện và tập kiểm tra
X_train, X_test, y_train, y_test = train_test_split(X_encoded, y, test_size=0.2, random_state=42)

# Huấn luyện mô hình Decision Tree
tree = DecisionTreeClassifier(random_state=42)
tree.fit(X_train, y_train)

# Dự đoán trên tập kiểm tra
y_pred = tree.predict(X_test)

# Đánh giá độ chính xác của mô hình
accuracy = accuracy_score(y_test, y_pred)
print("Độ chính xác của mô hình Decision Tree trên tập kiểm tra:", accuracy)


plt.figure(figsize=(20,10))
plot_tree(tree, feature_names=X_encoded.columns, class_names=True, filled=True)
plt.show()

plt.figure(figsize=(8, 6))
y_train.value_counts().plot(kind='bar', color='skyblue')
plt.xlabel('Class')
plt.ylabel('Count')
plt.title('Distribution of Classes in Training Data')
plt.xticks(rotation=45)
plt.show()

plt.figure(figsize=(8, 6))
y_test.value_counts().plot(kind='bar', color='lightgreen')
plt.xlabel('Class')
plt.ylabel('Count')
plt.title('Distribution of Classes in Test Data')
plt.xticks(rotation=45)
plt.show()
