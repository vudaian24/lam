import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score

# Đọc dữ liệu từ tệp CSV
df = pd.read_csv('C:\\Workspace\\Lâm\\Chatbox\\dataset.csv')

# Chia dữ liệu thành features (X) và labels (y)
X = df.drop('Type', axis=1)  # Loại bỏ cột Type, đây là nhãn
y = df['Type']

# Chia dữ liệu thành tập huấn luyện và tập kiểm tra theo tỷ lệ 2:8
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Huấn luyện mô hình KNN với k=5
knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)

# Dự đoán trên tập kiểm tra
y_pred = knn.predict(X_test)

# Đánh giá độ chính xác của mô hình
accuracy = accuracy_score(y_test, y_pred)
print("Độ chính xác của mô hình KNN trên tập kiểm tra:", accuracy)

# Vẽ biểu đồ Scatter Plot cho cặp feature đầu tiên và thứ hai
plt.figure(figsize=(10, 6))
plt.scatter(X.iloc[:, 0], X.iloc[:, 1], c=y, cmap='viridis', edgecolor='k')
plt.xlabel('Feature 1')
plt.ylabel('Feature 2')
plt.title('Scatter Plot of Feature 1 vs Feature 2')
plt.colorbar(label='Type')
plt.show()

# Vẽ biểu đồ Scatter Plot cho cặp feature thứ hai và thứ ba
plt.figure(figsize=(10, 6))
plt.scatter(X.iloc[:, 1], X.iloc[:, 2], c=y, cmap='viridis', edgecolor='k')
plt.xlabel('Feature 2')
plt.ylabel('Feature 3')
plt.title('Scatter Plot of Feature 2 vs Feature 3')
plt.colorbar(label='Type')
plt.show()

# Vẽ biểu đồ Scatter Plot cho cặp feature đầu tiên và thứ ba
plt.figure(figsize=(10, 6))
plt.scatter(X.iloc[:, 0], X.iloc[:, 2], c=y, cmap='viridis', edgecolor='k')
plt.xlabel('Feature 1')
plt.ylabel('Feature 3')
plt.title('Scatter Plot of Feature 1 vs Feature 3')
plt.colorbar(label='Type')
plt.show()