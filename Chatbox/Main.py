import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, roc_curve
from sklearn.multiclass import OneVsRestClassifier
import matplotlib.pyplot as plt
import seaborn as sns

# Đọc dữ liệu từ file CSV
data_path = 'C:\\Users\\VDA\\Downloads\\Chatbox\\dataset.csv'
df = pd.read_csv(data_path)

# Kiểm tra dữ liệu
print(df.head())

# Chọn các trường đặc trưng (features) và nhãn (label)
features = ['Material', 'Color', 'Type', 'Length', 'Season']
label = 'Style'

# Tiền xử lý dữ liệu nếu cần (ví dụ: mã hóa các biến phân loại)
df = pd.get_dummies(df, columns=features)

# Chia dữ liệu thành tập huấn luyện và tập kiểm tra theo tỷ lệ 8:2
X = df.drop(columns=[label])
y = df[label]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Tạo và huấn luyện mô hình SVM
model = SVC(probability=True)
model.fit(X_train, y_train)

# Dự đoán nhãn cho tập kiểm tra
y_pred = model.predict(X_test)

# Đánh giá mô hình
accuracy = accuracy_score(y_test, y_pred)
print(f'Tỉ lệ chính xác của mô hình: {accuracy:.2f}')
print(df.head())

# 1. Biểu đồ phân phối nhãn (Style)
plt.figure(figsize=(10, 6))
sns.countplot(data=df, x='Style')
plt.title('Phân phối của nhãn (Style)')
plt.xlabel('Style')
plt.ylabel('Số lượng')
plt.show()

# 2. Vẽ ROC Curve cho mô hình SVM với phương pháp One-vs-Rest
ovr_model = OneVsRestClassifier(SVC(probability=True))
ovr_model.fit(X_train, y_train)

y_proba_ovr = ovr_model.predict_proba(X_test)

plt.figure(figsize=(8, 6))  
for i in range(len(ovr_model.classes_)):
    fpr, tpr, _ = roc_curve(y_test == i, y_proba_ovr[:, i])
    plt.plot(fpr, tpr, label=f'Class {i}')

plt.plot([0, 1], [0, 1], color='navy', linestyle='--')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve (One-vs-Rest)')
plt.legend()
plt.show()
