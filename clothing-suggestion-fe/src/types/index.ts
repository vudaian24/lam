export interface LoginFormInputs {
    email: string;
    password: string;
}

export interface RegisterFormInputs {
    email: string;
    password: string;
    username: string;
    avatar: string;
}

export interface IClothing {
    id: string;
    name: string;
    description: string;
    price: number;
    quantity: number;
    Image: string;
    category: string;
    Color: number;
    brand: string;
    size: string;
    age: string;
}

export interface createClothingProps {
    name: string;
    description: string;
    price: number;
    quantity: number;
    image: string;
    category: string;
    color: string;
    brand: string;
    size: string;
    age: string;
}

export interface updateClothingPropsItem {
    name: string;
    description: string;
    price: number;
    quantity: number;
    image: string;
    category: string;
    color: string;
    brand: string;
    size: string;
    age: string;
}
export interface updateClothingProps {
    id: string;
    body: updateClothingPropsItem
}

export interface IUser {
    id: string;
    email: string;
    username: string;
    avatar: string;
    role: string;
}

export interface createUserProps {
    email: string;
    username: string;
    avatar: string;
    role: string;
}

export interface updateUserProps {
    id: string;
    body: createUserProps;
}

export interface ICategory {
    id: string;
    index: string,
    name: string;
    description: string;
}

export interface createCategoryProps {
    name: string;
    description: string;
}

export interface updateCategoryProps {
    id: string;
    body: createCategoryProps;
}

export interface IBrand {
    id: string;
    index: string,
    name: string;
    description: string;
}

export interface createBrandProps {
    name: string;
    description: string;
}

export interface updateBrandProps {
    id: string;
    body: createBrandProps;
}

export interface IColor {
    id: string;
    index: string,
    name: string;
    description: string;
}

export interface createColorProps {
    name: string;
    description: string;
}

export interface updateColorProps {
    id: string;
    body: createColorProps;
}

export interface ISize {
    id: string;
    index: string,
    name: string;
    description: string;
}

export interface createSizeProps {
    name: string;
    description: string;
}

export interface updateSizeProps {
    id: string;
    body: createSizeProps;
}

export interface ItemSuccess {
    Style: number;
    Image: string;
}