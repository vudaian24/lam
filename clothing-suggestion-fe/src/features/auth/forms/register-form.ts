import { useNavigate } from "react-router-dom";
import { useAuthStore } from "../stores";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { RegisterFormInputs } from "../interfaces";
import { registerSchema } from "../schema";
import { HttpStatus } from "../../../common/constants";
import Swal from "sweetalert2";

export const useRegisterForm = () => {
    const authStore = useAuthStore();
    const navigate = useNavigate();
    const {
        register, handleSubmit, formState: { errors }
    } = useForm<RegisterFormInputs>({
        resolver: yupResolver(registerSchema)
    });
    
    const onSubmit: SubmitHandler<RegisterFormInputs> = async (data) => {
        const res = await authStore.register({
            username: data.username,
            email: data.email,
            avatar: data.avatar,
            role: "user",
            password: data.password
        })

        if (res?.status === HttpStatus.OK) {
            Swal.fire({
                text: 'Đăng ký thành công!',
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            window.location.href = '/login'
        } else {
        }
    }
    
    return {
        register,
        handleSubmit, 
        onSubmit,
        errors
    };
};