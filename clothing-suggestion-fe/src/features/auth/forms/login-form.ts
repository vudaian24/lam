import { useForm, SubmitHandler } from "react-hook-form";
import { useAuthStore } from "../stores";
import { yupResolver } from "@hookform/resolvers/yup";
import { LoginFormInputs } from "../../../types";
import { loginWithPasswordSchema } from "../schema";
import Swal from "sweetalert2";
export const useLoginForm = () => {
    const authStore = useAuthStore();
    const {
        register, handleSubmit, formState: { errors }
    } = useForm<LoginFormInputs>({
        resolver: yupResolver(loginWithPasswordSchema)
    });

    const onSubmit: SubmitHandler<LoginFormInputs> = async (data) => {
        const res = await authStore.login({
            email: data.email,
            password: data.password
        });
        console.log(res);

        if (res.success) {
            Swal.fire({
                text: 'Đăng nhập thành công!',
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            window.location.href = '/'
        } else {
            Swal.fire({
                text: 'Sai email hoặc mật khẩu!',
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
        }
    };

    return {
        register,
        handleSubmit,
        onSubmit,
        errors
    };
};
