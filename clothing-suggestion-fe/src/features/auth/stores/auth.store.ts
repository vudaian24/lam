import { IBodyLogin, IBodyRegister } from "../interfaces";
import { authApi } from "../services";
import localStorageAuthService from '../../../common/storages/authStorage';

export const useAuthStore = () => {
    async function login(body: IBodyLogin) {
        const res = await authApi.login(body);
        console.log(res);
        
        if (res.success) {
            localStorageAuthService.setAccessToken(res.data?.accessToken);
            localStorageAuthService.setAccessTokenExpiredAt(res.data?.expiresIn);
        }
        return res;
    }

    async function register(body: IBodyRegister) {  
        try {
            const res = await authApi.register(body);
            return res;
        }
        catch (error) {
            console.log(error);
        }
    }

    const hasToken = () => {
        return !!localStorageAuthService.getAccessToken();
    };

    return {
        login,
        register,
        hasToken
    }
}