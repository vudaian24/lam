import { useDispatch } from "react-redux";
import { userApiService } from "../services";
import { UserProfile } from "../../../common/interfaces";
import { setAvatarUrl, setEmail, setId, setRole, setUserName } from "../../actions/userProfile";

export const useUserStore = () => {
    const dispatch = useDispatch();

    async function getUserProfile() {
        try {
            const res = await userApiService._getOwnProfile();
            if (res.success) {
                const userProfileData: UserProfile = res.data as UserProfile;
                dispatch(setId(userProfileData.id));
                dispatch(setUserName(userProfileData.username));
                dispatch(setEmail(userProfileData.email));
                dispatch(setAvatarUrl(userProfileData.avatar));
                dispatch(setRole(userProfileData.role));
            }
        } catch (error) {
            console.error("Error getting user profile:", error);
        }
    }

    return {
        getUserProfile
    };
};
