import { combineReducers, createStore } from "redux";
import userProfileReducer from "./userProfile";
import valueSearchReducer from "./searchItem";
import valueSearchCategoryReducer from "./searchCategory";

const rootReducer = combineReducers({
    userProfile: userProfileReducer,
    valueSearch: valueSearchReducer,
    valueSearchCategory: valueSearchCategoryReducer
});

const store = createStore(rootReducer);

export default store;
