const valueSearchReducer = (state = { value: '' }, action: any) => {
    switch (action.type) {
        case "SETSEARCHVALUE":
            return {
                ...state,
                value: action.payload
            };
        default:
            return state;
    }
};

export default valueSearchReducer;