const valueSearchCategoryReducer = (state = { value: '' }, action: any) => {
    switch (action.type) {
        case "SETSEARCHCATEGORY":
            return {
                ...state,
                value: action.payload
            };
        default:
            return state;
    }
};

export default valueSearchCategoryReducer;