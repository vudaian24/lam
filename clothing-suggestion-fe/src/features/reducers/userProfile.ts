const userProfileReducer = (state = {
    id: "",
    username: "",
    email: "",
    avatar: "",
    role: ""
}, action: any) => {
    switch (action.type) {
        case 'SETID':
            return {
                ...state,
                id: action.payload
            };
        case 'SETNAME':
            return {
                ...state,
                username: action.payload
            };
        case 'SETEMAIL':
            return {
                ...state,
                email: action.payload
            };
        case 'SETAVATARURL':
            return {
                ...state,
                avatar: action.payload
            };
        case 'SETROLE':
            return {
                ...state,
                role: action.payload
            };
        default:
            return state;
    }
};

export default userProfileReducer;
