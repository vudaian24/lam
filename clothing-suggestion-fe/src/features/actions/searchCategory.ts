export const setSearchCategory = (value: any) => ({
    type: "SETSEARCHCATEGORY",
    payload: value
});