export const setId = (id: any) => ({
    type: "SETID",
    payload: id
});

export const setUserName = (name: any) => ({
    type: "SETNAME",
    payload: name
});

export const setEmail = (email: any) => ({
    type: "SETEMAIL",
    payload: email
});

export const setAvatarUrl = (avatarUrl: any) => ({
    type: "SETAVATARURL",
    payload: avatarUrl
}); 
export const setRole = (role: any) => ({
    type: "SETROLE",
    payload: role
}); 
