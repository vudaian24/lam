const routes = {
  homePage: "/",
  chatPage: "/chat",
  loginPage: "/login",
  user: "/user",
  product: "/product",
  season: "/season",
  color: "/color",
  size: "/size",
  category: "/category",
  registerPage: "/register",
  notFoundPage: "/*",
  detailPage: "/detail/:id"
};
const config = {
  routes,
};

export default config;
