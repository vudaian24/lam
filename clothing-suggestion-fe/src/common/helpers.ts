import { Dispatch } from 'redux';
import { useDispatch } from 'react-redux';
import dayjs from './../plugins/dayjs/index';
import { DATE_TIME_FORMAT } from './constants';

export function formatDate(date: Date | string) {
    return dayjs(date).format(DATE_TIME_FORMAT.YYYY_MM_DD_DASH);
}

// Custom hook để sử dụng dispatch
export function useToastDispatch() {
    return useDispatch<Dispatch<any>>();
}

export function formatMoney(amount: any) {
    return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

