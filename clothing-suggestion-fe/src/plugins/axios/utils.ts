// import { useDispatch } from 'react-redux';
import { PageName } from '../../common/constants';
import localStorageAuthService from '../../common/storages/authStorage';

export const logout = (redirectToLogin = true) => {
  localStorageAuthService.resetAll();
  if (redirectToLogin) {
    const currentPage = sessionStorage.getItem('redirect') || '/';
    if (currentPage !== PageName.LOGIN_PAGE) {
      sessionStorage.setItem('redirect', currentPage);
      window.location.href = PageName.LOGIN_PAGE;
    }
  }
};
