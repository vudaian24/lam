import { Fragment, useEffect, useState } from 'react'
import { Disclosure, Menu, Transition } from '@headlessui/react'
import { Bars3Icon, BellIcon, XMarkIcon } from '@heroicons/react/24/outline'
import { logout } from '../../plugins/axios';
import { useDispatch, useSelector } from 'react-redux';
import { useUserStore } from '../../features/auth/stores/user.store';
import Footer from '../../components/Footer';
import { IBrand, ICategory } from '../../types';
import { getAllStyles } from '../../hooks/brand.hook';
import { setSearchCategory } from '../../features/actions/searchCategory';
import { getAllCategories } from '../../hooks/category.hook';

interface Props {
    children: React.ReactNode;
}

const navigation = [
    { name: 'Home', href: '/', current: true },
    { name: 'User consultation', href: '/chat', current: true },
]
const userNavigation = [
    { name: 'Your Profile', href: '#' },
    { name: 'Sign out', href: '#' },
]

function classNames(...classes: any) {
    return classes.filter(Boolean).join(' ')
}

export default function LayoutMain({
    children
}: Props) {
    const [isOpen, setIsOpen] = useState(false);
    const handleCancel = () => {
        setIsOpen(false);
    };
    const handleLogout = () => {
        logout();
        setIsOpen(false);
    };
    const userProfile = useSelector((state: any) => state.userProfile);
    const { getUserProfile } = useUserStore();
    useEffect(() => {
        getUserProfile();
    }, []);
    const [style, setStyle] = useState<ICategory[]>([]);
    const currentUrl = window.location.pathname;
    const navigationCategory = [
        { name: 'View all products', index: ''},
    ]

    useEffect(() => {
        async function fetchData() {
            try {
                const res = await getAllCategories('');
                setStyle(res?.data.items);
            } catch (error) {
                console.error("Error fetching users:", error);
            }
        }
        fetchData();
    }, []);
    const nameStyle = style.map((style) => {
        return {
            index: style.index,
            name: style.name,
        }
    })
    const combinedNavigationCategory = [
        ...navigationCategory,
        ...nameStyle,
    ];
    const dispatch = useDispatch();
    const handleSearch = async(val: any) => {
        if(val === "View all products") {
            await dispatch(setSearchCategory(''));
        } else {
            await dispatch(setSearchCategory(val));
        }
    }
    return (
        <>
            <div className="min-h-full">
                <Disclosure as="nav" className="border-b border-gray-200 bg-white">
                    {({ open }) => (
                        <>
                            <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                                <div className="flex h-16 justify-between">
                                    <div className="flex">
                                        <div className="flex flex-shrink-0 items-center">
                                            <img
                                                className="hidden h-10 w-auto lg:block"
                                                src="../../logo.png"
                                                alt="Your Company"
                                            />
                                        </div>
                                        <div className="hidden sm:-my-px sm:ml-6 sm:flex sm:space-x-8">
                                            {navigation.map((item) => (
                                                <a
                                                    key={item.name}
                                                    href={item.href}
                                                    className={classNames(
                                                        currentUrl === item.href
                                                            ? 'border-indigo-500 text-gray-900'
                                                            : 'border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700',
                                                        'inline-flex items-center border-b-2 px-1 pt-1 text-sm font-medium'
                                                    )}
                                                    aria-current={item.current ? 'page' : undefined}
                                                >
                                                    {item.name}
                                                </a>
                                            ))}
                                        </div>
                                    </div>
                                    <div className="hidden sm:ml-6 sm:flex sm:items-center">
                                        <button
                                            type="button"
                                            className="relative rounded-full bg-white p-1 text-gray-400 hover:text-gray-500"
                                        >
                                            <span className="absolute -inset-1.5" />
                                            <BellIcon className="h-6 w-6" aria-hidden="true" />
                                        </button>

                                        <Menu as="div" className="relative ml-3">
                                            <div>
                                                <Menu.Button className="relative flex max-w-xs items-center rounded-full bg-white text-sm">
                                                    <span className="absolute -inset-1.5" />
                                                    <img className="h-8 w-8 rounded-full" src={userProfile.avatar} alt="" />
                                                </Menu.Button>
                                            </div>
                                            <Transition
                                                as={Fragment}
                                                enter="transition ease-out duration-200"
                                                enterFrom="transform opacity-0 scale-95"
                                                enterTo="transform opacity-100 scale-100"
                                                leave="transition ease-in duration-75"
                                                leaveFrom="transform opacity-100 scale-100"
                                                leaveTo="transform opacity-0 scale-95"
                                            >
                                                <Menu.Items className="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                                                    <Menu.Item>
                                                        <button
                                                            onClick={handleLogout}
                                                            className='block px-4 py-2 text-sm text-gray-700'
                                                        >
                                                            Logout
                                                        </button>
                                                    </Menu.Item>
                                                </Menu.Items>
                                            </Transition>
                                        </Menu>
                                    </div>
                                    <div className="-mr-2 flex items-center sm:hidden">
                                        <Disclosure.Button className="relative inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                                            <span className="absolute -inset-0.5" />
                                            <span className="sr-only">Open main menu</span>
                                            {open ? (
                                                <XMarkIcon className="block h-6 w-6" aria-hidden="true" />
                                            ) : (
                                                <Bars3Icon className="block h-6 w-6" aria-hidden="true" />
                                            )}
                                        </Disclosure.Button>
                                    </div>
                                </div>
                            </div>

                            <Disclosure.Panel className="sm:hidden">
                                <div className="space-y-1 pb-3 pt-2">
                                    {navigation.map((item) => (
                                        <Disclosure.Button
                                            key={item.name}
                                            as="a"
                                            href={item.href}
                                            className={classNames(
                                                item.current
                                                    ? 'border-indigo-500 bg-indigo-50 text-indigo-700'
                                                    : 'border-transparent text-gray-600 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-800',
                                                'block border-l-4 py-2 pl-3 pr-4 text-base font-medium'
                                            )}
                                            aria-current={item.current ? 'page' : undefined}
                                        >
                                            {item.name}
                                        </Disclosure.Button>
                                    ))}
                                </div>
                                <div className="border-t border-gray-200 pb-3 pt-4">
                                    <div className="flex items-center px-4">
                                        <div className="flex-shrink-0">
                                            {userProfile.avatar ? (
                                                <img className="h-10 w-10 rounded-full" src='./icons/ic_user.svg' alt="avatar" />
                                            ) : (
                                                <img className="h-10 w-10 rounded-full" src={userProfile.avatar} alt="avatar" />
                                            )}
                                        </div>
                                        <div className="ml-3">
                                            <div className="text-base font-medium text-gray-800">{userProfile.username}</div>
                                            <div className="text-sm font-medium text-gray-500">{userProfile.email}</div>
                                        </div>
                                        <button
                                            type="button"
                                            className="relative ml-auto flex-shrink-0 rounded-full bg-white p-1 text-gray-400 hover:text-gray-500"
                                        >
                                            <span className="absolute -inset-1.5" />
                                            <BellIcon className="h-6 w-6" aria-hidden="true" />
                                        </button>
                                    </div>
                                    <div className="mt-3 space-y-1">
                                        {userNavigation.map((item) => (
                                            <Disclosure.Button
                                                key={item.name}
                                                as="a"
                                                href={item.href}
                                                className="block px-4 py-2 text-base font-medium text-gray-500 hover:bg-gray-100 hover:text-gray-800"
                                            >
                                                {item.name}
                                            </Disclosure.Button>
                                        ))}
                                    </div>
                                </div>
                            </Disclosure.Panel>
                        </>
                    )}
                </Disclosure>

                <div className="py-10 px-8 mx-auto max-w-7xl flex gap-10">
                    <div className='sm:w-[100px] md:w-[180px]'>
                        <nav className="flex flex-1 flex-col" aria-label="Sidebar">
                            <ul role="list" className="-mx-2 space-y-1">
                                {combinedNavigationCategory.map((item) => (
                                    <li key={item.name}>
                                        <button
                                            onClick={() => handleSearch(item.index)}
                                            className="group flex gap-x-3 rounded-md p-2 pl-3 text-sm leading-6 font-semibold hover:bg-gray-50"
                                        >
                                            {item.name}
                                        </button>
                                    </li>
                                ))}
                            </ul>
                        </nav>
                    </div>
                    <main className='flex-1'>
                        <div className="sm:px-3 lg:px-4">{children}</div>
                    </main>
                </div>
                <Footer />
            </div>
        </>
    )
}
