import React from 'react';

interface Props {
    children: React.ReactNode;
}

const Layout = ({ children }: Props) => {

    return (
        <div className='grid grid-cols-1 sm:grid-cols-2  w-screen h-screen'>
            <div className='flex flex-col'>
                {children}
            </div>
            <div className="sm:hidden lg:block">
                <img
                    className="h-full w-full object-cover"
                    src="../../images/bg.jpg"
                    alt=""
                />
            </div>
        </div>
    );
};

export default Layout;