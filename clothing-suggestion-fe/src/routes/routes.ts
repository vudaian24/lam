import config from '../config';
import LoginPage from '../pages/Login';
import RegisterPage from '../pages/Register';
import NotFoundPage from '../features/errors/pages/NotFoundPage';
import Layout from '../layouts';
import HomePage from '../pages/Home';
import LayoutMain from '../layouts/Layout';
import ChatPage from '../pages/Chat';
import LayoutDashboard from '../layouts/LayoutDashboard';
import ProductPage from '../pages/Product';
import UserPage from '../pages/User';
import CategoryPage from '../pages/Category';
import SeasonPage from '../pages/Season';
import ColorPage from '../pages/Color';
import SizePage from '../pages/Size';
import DetailProduct from '../pages/DetailProduct';

const publicRoutes = [
    { path: config.routes.homePage, component: HomePage, element: LayoutMain },
    { path: config.routes.chatPage, component: ChatPage, element: LayoutMain },
    { path: config.routes.loginPage, component: LoginPage, element: Layout },
    { path: config.routes.registerPage, component: RegisterPage, element: Layout },
    { path: config.routes.product, component: ProductPage, element: LayoutDashboard },
    { path: config.routes.user, component: UserPage, element: LayoutDashboard },
    { path: config.routes.category, component: CategoryPage, element: LayoutDashboard },
    { path: config.routes.season, component: SeasonPage, element: LayoutDashboard },
    { path: config.routes.color, component: ColorPage, element: LayoutDashboard },
    { path: config.routes.size, component: SizePage, element: LayoutDashboard },
    { path: config.routes.notFoundPage, component: NotFoundPage, element: Layout },
    { path: config.routes.detailPage, component: DetailProduct, element: LayoutMain },
];

export { publicRoutes };
