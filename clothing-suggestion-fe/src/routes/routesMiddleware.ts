import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import localStorageAuthService from '../common/storages/authStorage';
import { PageName } from '../common/constants';
import { useSelector } from 'react-redux';

const useAuthMiddleware = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const tokenExpiredAt = localStorageAuthService.getAccessTokenExpiredAt();
    const hasToken = localStorageAuthService.getAccessToken() ? true : false;
    const isAuthenticated = tokenExpiredAt && hasToken;
    const currentPath = location.pathname;
    const userProfile = useSelector((state: any) => state.userProfile);
    useEffect(() => {
        
        if (isAuthenticated && (currentPath === PageName.LOGIN_PAGE || currentPath === PageName.REGISTER_PAGE)) {
            navigate(PageName.HOME_PAGE, { replace: true });
        }
        else if (!isAuthenticated && currentPath !== PageName.LOGIN_PAGE && currentPath !== PageName.REGISTER_PAGE) {
            navigate(PageName.LOGIN_PAGE, { replace: true });
        }

        if (userProfile.role === 'admin') {
            if (currentPath !== '/user' && currentPath !== '/product' && currentPath !== '/season' && currentPath !== '/color' && currentPath !== '/size' && currentPath !== '/category') {
                navigate('/user');
            }
        }

        if (userProfile.role === 'user') {
            if (currentPath === '/user' || currentPath === '/product') {
                navigate('/');
            }
        }

    }, [isAuthenticated, currentPath, navigate, userProfile]);
};

export default useAuthMiddleware;
