import React from 'react';
import { Route, Routes } from 'react-router-dom';
import useRouteMiddleware from './routesMiddleware';
import { publicRoutes } from './routes';
const AppRouter = () => {

    useRouteMiddleware();

    return (
        <Routes>
            {publicRoutes.map((route, index) => {
                const Page = route.component;
                const Layout = route.element;
                return (
                    <Route
                        key={index}
                        path={route.path}
                        element={
                            <Layout>
                                <Page />
                            </Layout>
                        }
                    />
                );
            })}
        </Routes>
    );
};

export default AppRouter;
