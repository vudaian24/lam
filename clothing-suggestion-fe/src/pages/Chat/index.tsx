import React, { useEffect, useState } from 'react';
import { CheckIcon } from '@heroicons/react/24/solid'
import { getAllBrands } from '../../hooks/brand.hook';
import { useSelector } from 'react-redux';
import { IBrand, ICategory, IColor, ISize, ItemSuccess } from '../../types';
import { getAllColors } from '../../hooks/color.hook';
import { getAllSizes } from '../../hooks/size.hook';
import { getAllCategories } from '../../hooks/category.hook';
import axios from 'axios';
import Loading from '../../components/Loading';
import ShowProduct from '../../components/ShowProduct';
import CustomDialog from '../../components/Dialog';

const ChattingPage = () => {
    const [activeIndex, setActiveIndex] = useState(0);
    const [currentStep, setCurrentStep] = useState(0);
    const [completedSteps, setCompletedSteps] = useState<number[]>([]);
    const valueSearch = useSelector((state: any) => state.valueSearch);
    const [brands, setBrands] = useState<IBrand[]>([]);
    const [colors, setColors] = useState<IColor[]>([]);
    const [sizes, setSizes] = useState<ISize[]>([]);
    const [categories, setCategories] = useState<ICategory[]>([]);
    const [loading, setLoading] = useState(false)
    const steps = [
        { id: '01', name: 'Job details' },
        { id: '02', name: 'Application form' },
        { id: '03', name: 'Preview' },
    ]

    const handleStepClick = (stepIndex: number) => {
        setCurrentStep(stepIndex);
        setCompletedSteps([...completedSteps.slice(0, stepIndex), stepIndex]);
    };
    const [itemSucceeded, setItemSucceeded] = useState<ItemSuccess[]>([])
    useEffect(() => {
        async function fetchData() {
            try {
                const resStyle = await getAllBrands(valueSearch.value as string);
                const resColor = await getAllColors(valueSearch.value as string);
                const resSize = await getAllSizes(valueSearch.value as string);
                const resCategory = await getAllCategories(valueSearch.value as string);
                setCategories(resCategory.data.items);
                setSizes(resSize.data.items);
                setColors(resColor.data.items);
                setBrands(resStyle.data.items);
            } catch (error) {
                console.error("Error fetching brands:", error);
            }
        }
        fetchData();

    }, []);

    const [selectedSeason, setSelectedSeason] = useState<string>('');
    const [selectedSize, setSelectedSize] = useState<string>('');
    const [selectedMaterial, setSelectedMaterial] = useState<string>('');
    const [selectedColor, setSelectedColor] = useState<string>('');
    const [selectedCategory, setSelectedCategory] = useState<string>('');

    const handleSeasonChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setSelectedSeason(event.target.value);
    };

    const handleSizeChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setSelectedSize(event.target.value);
    };

    const handleMaterialChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setSelectedMaterial(event.target.value);
    };

    const handleColorChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setSelectedColor(event.target.value);
    };

    const handleCategoryChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setSelectedCategory(event.target.value);
    };

    const handleSubmit = async () => {
        try {
            setLoading(true);
            const body = {
                Material: selectedMaterial,
                Color: selectedColor,
                Category: selectedCategory,
                Size: selectedSize,
                Season: selectedSeason
            }
            const res = await axios.post(`http://127.0.0.1:8000/items/`, body)
            setItemSucceeded(res.data)
            setLoading(false);
        } catch (error) {
            console.error("Error fetching brands:", error);
        }
    };

    
    return (
        <div className="mx-auto max-w-7xl">
            {
                loading ? <Loading /> : (
                    <>
                        {
                            itemSucceeded.length > 0 ? (
                                <>
                                    <ShowProduct data={itemSucceeded} />
                                </>
                            ) : (
                                <div className={` mt-10`}>
                                    <div className='w-full h-[500px] bg-white rounded-md p-10 pt-20 border-[1px] border-solid border-[#ccc]'>
                                        <div className='grid grid-cols-2 gap-5'>
                                            <div className='col-span-1 flex flex-col gap-7'>
                                                <div>
                                                    <label htmlFor="location" className="block text-sm font-medium leading-6 text-gray-900">
                                                        Season
                                                    </label>
                                                    <select
                                                        id="location"
                                                        name="location"
                                                        onChange={handleSeasonChange}
                                                        className="mt-2 block w-full rounded-md border-0 py-3 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 outline-none sm:text-sm sm:leading-6"
                                                    >
                                                        {brands && brands.map((item, index) => (
                                                            <option value={item.index} key={index}>{item.name}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                                <div>
                                                    <label htmlFor="location" className="block text-sm font-medium leading-6 text-gray-900">
                                                        Size
                                                    </label>
                                                    <select
                                                        id="location"
                                                        name="location"
                                                        onChange={handleSizeChange}
                                                        className="mt-2 block w-full rounded-md border-0 py-3 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 outline-none sm:text-sm sm:leading-6"
                                                    >
                                                        {sizes && sizes.map((item, index) => (
                                                            <option value={item.index} key={index}>{item.name}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                                <div>
                                                    <label htmlFor="material" className="block text-sm font-medium leading-6 text-gray-900">
                                                        Material
                                                    </label>
                                                    <select
                                                        id="material"
                                                        name="material"
                                                        onChange={handleMaterialChange}
                                                        className="mt-2 block w-full rounded-md border-0 py-3 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 outline-none sm:text-sm sm:leading-6"
                                                    >
                                                        <option value="1">Cotton</option>
                                                        <option value="2">Wool</option>
                                                        <option value="3">Silk</option>
                                                        <option value="4">Linen</option>
                                                        <option value="5">Polyester</option>
                                                        <option value="6">Nylon</option>
                                                        <option value="7">Rayon</option>
                                                        <option value="8">Spandex</option>
                                                        <option value="9">Denim</option>
                                                        <option value="10">Leather</option>
                                                    </select>
                                                </div>

                                            </div>
                                            <div className='col-span-1 flex flex-col gap-7'>
                                                <div>
                                                    <label htmlFor="location" className="block text-sm font-medium leading-6 text-gray-900">
                                                        Color
                                                    </label>
                                                    <select
                                                        id="location"
                                                        name="location"
                                                        onChange={handleColorChange}
                                                        className="mt-2 block w-full rounded-md border-0 py-3 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 outline-none sm:text-sm sm:leading-6"
                                                    >
                                                        {colors && colors.map((item, index) => (
                                                            <option value={item.index} key={index}>{item.name}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                                <div>
                                                    <label htmlFor="location" className="block text-sm font-medium leading-6 text-gray-900">
                                                        Type
                                                    </label>
                                                    <select
                                                        id="location"
                                                        name="location"
                                                        onChange={handleCategoryChange}
                                                        className="mt-2 block w-full rounded-md border-0 py-3 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 outline-none sm:text-sm sm:leading-6"
                                                    >
                                                        {categories && categories.map((item, index) => (
                                                            <option value={item.index} key={index}>{item.name}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='flex items-center justify-end mt-8'>
                                            <button
                                                type="button"
                                                onClick={handleSubmit}
                                                className="rounded-md bg-blue-500 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                            >
                                                Send
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </>

                )
            }
        </div>
    );
};

export default ChattingPage;
