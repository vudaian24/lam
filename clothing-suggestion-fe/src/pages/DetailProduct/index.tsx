import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { IClothing } from '../../types';
import { getClothingDetail } from '../../hooks/clothing.hook';
import { CheckIcon } from '@heroicons/react/20/solid'
import { formatMoney } from '../../common/helpers';

const DetailProduct = () => {
    let { id } = useParams();
    const [clothing, setClothing] = useState<IClothing>();
    useEffect(() => {
        async function fetchDetail() {
            try {
                if (id) {
                    const response = await getClothingDetail(id);
                    setClothing(response.data);
                }
            } catch (error) {
                console.error("Error fetching users:", error);
            }
        }
        fetchDetail();
    }, [id]);
    return (
        <div className="bg-white ">
            {clothing && (
                <div className="mx-auto max-w-3xl px-4 py-16 sm:px-6 sm:py-24 lg:grid lg:grid-cols-2 lg:gap-x-8 lg:px-8 border-[1px] border-gray-300 border-solid rounded-2xl">
                    <div className="lg:max-w-lg lg:self-end">
                        <div className="mt-4">
                            <h1 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">{clothing.name}</h1>
                        </div>
                        <section aria-labelledby="information-heading" className="mt-4">
                            <h2 id="information-heading" className="sr-only">
                                Product information
                            </h2>
                            <div className="flex items-center">
                                <p className="text-lg text-gray-900 sm:text-xl">{formatMoney(clothing.price)} VND</p>
                            </div>
                            <div className="mt-4 space-y-6">
                                <p className="text-base text-gray-500">{clothing.description}</p>
                            </div>
                            <div className="mt-6 flex items-center">
                                <CheckIcon className="h-5 w-5 flex-shrink-0 text-green-500" aria-hidden="true" />
                                <p className="ml-2 text-sm text-gray-500">In stock and ready to ship</p>    
                            </div>
                        </section>
                    </div>
                    <div className="mt-10 lg:col-start-2 lg:row-span-2 lg:mt-0 lg:self-center">
                        <div className="aspect-h-1 aspect-w-1 overflow-hidden rounded-lg">
                            <img src={clothing.Image} alt='clothing image' className="h-full w-full object-cover object-center" />
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default DetailProduct;