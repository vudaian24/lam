import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import CustomDialog from '../../components/Dialog';
import { IBrand, ICategory, IClothing, IColor, ISize } from '../../types';
import { createClothing, deleteClothing, getAllClothings, updateClothing } from '../../hooks/clothing.hook';
import { formatMoney } from '../../common/helpers';
import { getAllCategories } from '../../hooks/category.hook';
import { getAllColors } from '../../hooks/color.hook';
import { getAllBrands } from '../../hooks/brand.hook';
import { getAllSizes } from '../../hooks/size.hook';
import Toast from '../../components/Toast';
import ConfirmationDialog from '../../components/ConfirmationDialog';

const ProductPage = () => {
    const [open, setOpen] = useState(false);
    const [clothing, setClothing] = useState<IClothing[]>([]);
    const [category, setCategory] = useState<ICategory[]>([]);
    const [brand, setBrand] = useState<IBrand[]>([]);
    const [color, setColor] = useState<IColor[]>([]);
    const [size, setSize] = useState<ISize[]>([]);
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [isLoad, setIsLoad] = useState(true);
    const valueSearch = useSelector((state: any) => state.valueSearch);
    const [formData, setFormData] = useState({
        id: '',
        name: '',
        description: '',
        price: 0,
        quantity: 0,
        Image: '',
        category: '',
        Color: 0,
        brand: '',
        size: '',
    });
    useEffect(() => {
        async function fetchCategory() {
            try {
                const response = await getAllCategories(valueSearch.value as string);
                setCategory(response.data.items);
            } catch (error) {
                console.error("Error fetching:", error);
            }
        }
        async function fetchColor() {
            try {
                const response = await getAllColors(valueSearch.value as string);
                setColor(response.data.items);
            } catch (error) {
                console.error("Error fetching:", error);
            }
        }
        async function fetchBrand() {
            try {
                const response = await getAllBrands(valueSearch.value as string);
                setBrand(response.data.items);
            } catch (error) {
                console.error("Error fetching:", error);
            }
        }
        async function fetchSize() {
            try {
                const response = await getAllSizes(valueSearch.value as string);
                setSize(response.data.items);
            } catch (error) {
                console.error("Error fetching:", error);
            }
        }
        async function fetchClothing() {
            try {
                const response = await getAllClothings(valueSearch.value as string);
                setClothing(response.data.items);
            } catch (error) {
                console.error("Error fetching clothing:", error);
            }
        }
        fetchClothing();
        fetchBrand();
        fetchCategory();
        fetchColor();
        fetchSize();
    }, [isLoad, valueSearch]);

    const handleClose = () => {
        setOpen(false);
        setIsEditModalOpen(false);
        setFormData({
            id: '',
            name: '',
            description: '',
            price: 0,
            quantity: 0,
            Image: '',
            category: '',
            Color: 0,
            brand: '',
            size: '',
        });
    };
    const handleChange = (e: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };
    const handleOpenEditModal = (Image: IClothing) => {
        setFormData(Image);
        setIsEditModalOpen(true);
    };
    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            if (formData.id) {
                await updateClothing(formData.id, formData);
            } else {
                await createClothing(formData);
            }
            handleClose();
            setIsLoad(!isLoad);
            handleShowToast("Lưu thành công", "success")
        } catch (error) {
            console.error("Error creating clothing:", error);
            handleShowToast("Lưu thất bại", "danger")
        }
    };
    const [deleteId, setDeleteId] = useState<string>('');
    const [openConfirmDelete, setOpenConfirmDelete] = useState(false);
    const handleRemove = async (id: string) => {
        setDeleteId(id);
        setOpenConfirmDelete(true);
    };

    const handleConfirmDelete = async () => {
        try {
            await deleteClothing(deleteId);
            setIsLoad(!isLoad);
        } catch (error) {
            console.error("Error deleting product:", error);
            handleShowToast('Xóa thất bại', "danger")
        } finally {
            setOpenConfirmDelete(false);
            handleShowToast("Xóa thành công", "success")
        }
    };

    const handleCloseDeleteConfirm = () => {
        setOpenConfirmDelete(false);
    };

    const [showToast, setShowToast] = useState(false);
    const [message, setMessage] = useState('');
    const [type, setType] = useState('');

    const handleShowToast = (msg: any, t: any) => {
        setMessage(msg);
        setType(t);
        setShowToast(true);
    };

    return (
        <div>
            <Toast message={message} type={type} showToast={showToast} setShowToast={setShowToast} />
            <ConfirmationDialog
                isOpen={openConfirmDelete}
                message="Are you sure you want to delete this product?"
                onConfirm={handleConfirmDelete}
                onCancel={handleCloseDeleteConfirm}
            />
            <div className="sm:flex sm:items-center">
                <div className="sm:flex-auto">
                    <h1 className="text-base font-semibold leading-6 text-gray-900">Clothing</h1>
                </div>
                <div className="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
                    <button
                        onClick={() => setOpen(true)}
                        type="button"
                        className="rounded-md bg-white px-3.5 py-2.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-blue-100"
                    >
                        Add clothing
                    </button>
                </div>
                <CustomDialog isOpen={open || isEditModalOpen} onClose={handleClose}>
                    <form className="w-full flex flex-col gap-5" onSubmit={handleSubmit}>
                        <div>
                            <label htmlFor="name" className="block text-sm font-medium leading-6 text-gray-900">
                                Name
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    name="name"
                                    value={formData.name}
                                    onChange={handleChange}
                                    required
                                    className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                    placeholder="Product name"
                                />
                            </div>
                        </div>
                        <div>
                            <label htmlFor="description" className="block text-sm font-medium leading-6 text-gray-900">
                                Description
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    name="description"
                                    value={formData.description}
                                    onChange={handleChange}
                                    required
                                    className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                    placeholder="Product description"
                                />
                            </div>
                        </div>
                        <div className='grid grid-cols-2 gap-5'>
                            <div>
                                <label htmlFor="price" className="block text-sm font-medium leading-6 text-gray-900">
                                    Price
                                </label>
                                <div className="mt-2">
                                    <input
                                        type="number"
                                        name="price"
                                        value={formData.price}
                                        onChange={handleChange}
                                        required
                                        className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                        placeholder="Product price"
                                    />
                                </div>
                            </div>
                            <div>
                                <label htmlFor="quantity" className="block text-sm font-medium leading-6 text-gray-900">
                                    Quantity
                                </label>
                                <div className="mt-2">
                                    <input
                                        type="number"
                                        name="quantity"
                                        value={formData.quantity}
                                        onChange={handleChange}
                                        required
                                        className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                        placeholder="Product quantity"
                                    />
                                </div>
                            </div>
                        </div>
                        <div>
                            <label htmlFor="image" className="block text-sm font-medium leading-6 text-gray-900">
                                Image URL
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    name="image"
                                    value={formData.Image}
                                    onChange={handleChange}
                                    required
                                    className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                    placeholder="Product image URL"
                                />
                            </div>
                        </div>
                        <div className='grid grid-cols-2 gap-5'>
                            <div>
                                <label htmlFor="category" className="block text-sm font-medium leading-6 text-gray-900">
                                    Category
                                </label>
                                <div className="mt-2">
                                    <select
                                        name="category"
                                        value={formData.category || (category.length > 0 ? category[0].name : '')}
                                        onChange={handleChange}
                                        required
                                        className="w-full pl-4 pr-5 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2.5 text-gray-900 placeholder:text-gray-400 outline-none"
                                    >
                                        {category && category.map((item, index) => (
                                            <option key={index} value={item.name}>
                                                {item.name}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                            <div>
                                <label htmlFor="color" className="block text-sm font-medium leading-6 text-gray-900">
                                    Color
                                </label>
                                <div className="mt-2">
                                    <select
                                        name="color"
                                        value={formData.Color || (color.length > 0 ? color[0].name : '')}
                                        onChange={handleChange}
                                        required
                                        className="w-full pl-4 pr-5 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2.5 text-gray-900 placeholder:text-gray-400 outline-none"
                                    >
                                        {color && color.map((item, index) => (
                                            <option key={index} value={item.name}>
                                                {item.name}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div>
                            <label htmlFor="brand" className="block text-sm font-medium leading-6 text-gray-900">
                                Style
                            </label>
                            <div className="mt-2">
                                <select
                                    name="brand"
                                    value={formData.brand || (brand.length > 0 ? brand[0].name : '')}
                                    onChange={handleChange}
                                    required
                                    className="w-full pl-4 pr-5 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2.5 text-gray-900 placeholder:text-gray-400 outline-none"
                                >
                                    {brand && brand.map((item, index) => (
                                        <option key={index} value={item.name}>
                                            {item.name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        <div>
                            <div>
                                <label htmlFor="size" className="block text-sm font-medium leading-6 text-gray-900">
                                    Size
                                </label>
                                <div className="mt-2">
                                    <select
                                        name="size"
                                        value={formData.size || (size.length > 0 ? size[0].name : '')}
                                        onChange={handleChange}
                                        required
                                        className="w-full pl-4 pr-5 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2.5 text-gray-900 placeholder:text-gray-400 outline-none"
                                    >
                                        {size && size.map((item, index) => (
                                            <option key={index} value={item.name}>
                                                {item.name}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div className="flex items-center justify-end py-5">
                            <button
                                type="submit"
                                className="rounded-md bg-white px-3.5 py-2.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-blue-100"
                            >
                                Save
                            </button>
                        </div>
                    </form>
                </CustomDialog>
            </div>
            <div className="mt-8 flow-root">
                <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                        <table className="min-w-full divide-y divide-gray-300">
                            <thead>
                                <tr>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Name
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Description
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Price
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Quantity
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Image
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Category
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Color
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Style
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Size
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="bg-white">
                                {clothing.map((item, index) => (
                                    <tr key={item.id} className={index % 2 === 0 ? 'even:bg-gray-50' : ''}>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.name}</td>
                                        <td className="max-w-[180px] min-w-[100px] px-1 py-4 text-sm text-gray-500">{item.description}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{formatMoney(item.price)}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.quantity}</td>
                                        <td className="whitespace-nowrap pl-1 pr-2 py-4 text-sm text-gray-500 max-w-[200px] overflow-hidden">
                                            <div className="h-20 w-20 flex-shrink-0">
                                                <img className="h-20 w-20 rounded-xl" src={item.Image} alt="avatar" />
                                            </div>
                                        </td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.category}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.Color}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.brand}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.size}</td>
                                        <td className="relative whitespace-nowrap py-4 pl-1 pr-4 text-sm font-medium sm:pr-3">
                                            <button
                                                onClick={() => handleOpenEditModal(item)}
                                                type="button"
                                                className="mr-2 inline-flex items-center rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 disabled:cursor-not-allowed disabled:opacity-30 disabled:hover:bg-white"
                                            >
                                                Edit
                                            </button>
                                            <button
                                                onClick={() => handleRemove(item.id)}
                                                type="button"
                                                className="inline-flex items-center rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 disabled:cursor-not-allowed disabled:opacity-30 disabled:hover:bg-white"
                                            >
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductPage;