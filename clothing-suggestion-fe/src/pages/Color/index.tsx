import React, { useEffect, useState } from 'react';
import Toast from '../../components/Toast';
import ConfirmationDialog from '../../components/ConfirmationDialog';
import { deleteColor } from '../../hooks/color.hook'; // Import các hàm hook của màu sắc
import { useSelector } from 'react-redux';
import { IColor } from '../../types'; // Import loại dữ liệu của màu sắc
import { createColor, getAllColors, updateColor } from '../../hooks/color.hook'; // Import các hàm hook của màu sắc
import CustomDialog from '../../components/Dialog';

const ColorPage = () => {
    const [open, setOpen] = useState(false);
    const [openConfirmDelete, setOpenConfirmDelete] = useState(false);
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [isLoad, setIsLoad] = useState(true);
    const [colors, setColors] = useState<IColor[]>([]); // Sử dụng loại dữ liệu của màu sắc
    const valueSearch = useSelector((state: any) => state.valueSearch);
    const [deleteId, setDeleteId] = useState<string>('');
    const [formData, setFormData] = useState({
        id: '',
        index: '',
        name: '',
        description: ''
    });
    useEffect(() => {
        async function fetchData() {
            try {
                const response = await getAllColors(valueSearch.value as string);
                setColors(response.data.items);
            } catch (error) {
                console.error("Error fetching colors:", error);
            }
        }
        fetchData();
    }, [isLoad, valueSearch]);

    const handleOpenEditModal = (item: IColor) => {
        setFormData(item);
        setIsEditModalOpen(true);
    };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleClose = () => {
        setOpen(false);
        setIsEditModalOpen(false);
        setFormData({
            id: '',
            index: '',
            name: '',
            description: ''
        });
    };

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            if (formData.id) {
                await updateColor(formData.id, formData);
            } else {
                await createColor({
                    index: formData.index,
                    name: formData.name,
                    description: formData.description
                });
            }
            handleClose();
            setIsLoad(!isLoad);
            handleShowToast("Lưu thành công", "success")
        } catch (error) {
            console.error("Error creating color:", error);
            handleShowToast("Lưu thất bại", "danger")
        }
    };

    const handleRemove = async (id: string) => {
        setDeleteId(id);
        setOpenConfirmDelete(true);
    };

    const handleConfirmDelete = async () => {
        try {
            await deleteColor(deleteId);
            setIsLoad(!isLoad);
        } catch (error) {
            console.error("Error deleting color:", error);
            handleShowToast('Xóa thất bại', "danger")
        } finally {
            setOpenConfirmDelete(false);
            handleShowToast("Xóa thành công", "success")
        }
    };

    const handleCloseDeleteConfirm = () => {
        setOpenConfirmDelete(false);
    };

    const [showToast, setShowToast] = useState(false);
    const [message, setMessage] = useState('');
    const [type, setType] = useState('');

    const handleShowToast = (msg: any, t: any) => {
        setMessage(msg);
        setType(t);
        setShowToast(true);
    };

    return (
        <div>
            <Toast message={message} type={type} showToast={showToast} setShowToast={setShowToast} />
            <ConfirmationDialog
                isOpen={openConfirmDelete}
                message="Are you sure you want to delete this color?"
                onConfirm={handleConfirmDelete}
                onCancel={handleCloseDeleteConfirm}
            />

            <div className="sm:flex sm:items-center">
                <div className="sm:flex-auto">
                    <h1 className="text-base font-semibold leading-6 text-gray-900">Color</h1>
                </div>
                <div className="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
                    <button
                        onClick={() => setOpen(true)}
                        type="button"
                        className="rounded-md bg-white px-3.5 py-2.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-blue-100"
                    >
                        Add color
                    </button>
                </div>
            </div>

            <CustomDialog isOpen={open || isEditModalOpen} onClose={handleClose}>
                <form className="w-full flex flex-col gap-5" onSubmit={handleSubmit}>
                    <div>
                        <label htmlFor="index" className="block text-sm font-medium leading-6 text-gray-900">
                            Index
                        </label>
                        <div className="mt-2">
                            <input
                                type="text"
                                name="index"
                                value={formData.index}
                                onChange={handleChange}
                                required
                                className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                placeholder="Index"
                            />
                        </div>
                    </div>
                    <div>
                        <label htmlFor="name" className="block text-sm font-medium leading-6 text-gray-900">
                            Name
                        </label>
                        <div className="mt-2">
                            <input
                                type="text"
                                name="name"
                                value={formData.name}
                                onChange={handleChange}
                                required
                                className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                placeholder="Name"
                            />
                        </div>
                    </div>
                    <div>
                        <label htmlFor="description" className="block text-sm font-medium leading-6 text-gray-900">
                            Description
                        </label>
                        <div className="mt-2">
                            <input
                                type="text"
                                name="description"
                                value={formData.description}
                                onChange={handleChange}
                                required
                                className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                placeholder="Description"
                            />
                        </div>
                    </div>
                    <div className="flex items-center justify-end py-5">
                        <button
                            type="submit"
                            className="rounded-md bg-white px-3.5 py-2.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-blue-100"
                        >
                            Save
                        </button>
                    </div>
                </form>
            </CustomDialog>

            <div className="mt-8 flow-root">
                <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                        <table className="min-w-full divide-y divide-gray-300">
                            <thead>
                                <tr>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Index
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Name
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Code
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="bg-white">
                                {colors.map((item, index) => (
                                    <tr key={item.id} className={index % 2 === 0 ? 'even:bg-gray-50' : ''}>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.index}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.name}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.description}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">
                                            <div style={{ backgroundColor: item.description }} className="w-12 h-8 rounded-md"></div>
                                        </td>
                                        <td className="relative whitespace-nowrap py-4 pl-1 pr-4 text-sm font-medium sm:pr-3">
                                            <button
                                                onClick={() => handleOpenEditModal(item)}
                                                type="button"
                                                className="mr-2 inline-flex items-center rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 disabled:cursor-not-allowed disabled:opacity-30 disabled:hover:bg-white"
                                            >
                                                Edit
                                            </button>
                                            <button
                                                onClick={() => handleRemove(item.id)}
                                                type="button"
                                                className="inline-flex items-center rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 disabled:cursor-not-allowed disabled:opacity-30 disabled:hover:bg-white"
                                            >
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default ColorPage;
