import React, { useEffect } from 'react';
import { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { XMarkIcon, Bars3Icon } from '@heroicons/react/24/outline'
import { getAllClothings, getProductByCategory } from '../../hooks/clothing.hook';
import { IBrand, IClothing, IColor } from '../../types';
import { formatMoney } from '../../common/helpers';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getAllBrands, getAllStyles } from '../../hooks/brand.hook';
import Loading from '../../components/Loading';
import CustomDialog from '../../components/Dialog';
import { getAllColors } from '../../hooks/color.hook';

const HomePage = () => {
    const [open, setOpen] = useState(false);
    const [clothing, setClothing] = useState<IClothing[]>([]);
    const valueSearch = useSelector((state: any) => state.valueSearchCategory);
    const [loading, setLoading] = useState(false)
    const [colors, setColors] = useState<IColor[]>([]);
    useEffect(() => {
        async function fetchImages() {
            try {
                setLoading(true);
                const response = await getProductByCategory(valueSearch.value);
                const res = await getAllColors('');
                setColors(res.data.items);
                setClothing(response?.data);
                setLoading(false);
            } catch (error) {
                console.error("Error fetching users:", error);
            }
        }
        fetchImages();
    }, [valueSearch]);
    const [isOpen, setIsOpen] = useState(false);
    const [selectItem, setSelectItem] = useState<any>(null);
    const handleOpen = async(item: any) => {
        setIsOpen(true);
        setSelectItem(item);
    }
    const handleClose = () => {
        setIsOpen(false);
    }
    return (
        <>
            {
                loading ? <Loading /> : (
                    <div className="bg-white">
                        <CustomDialog isOpen={isOpen} onClose={handleClose}>
                            {selectItem && (
                                <div>
                                    <img src={selectItem?.Image} alt="" />
                                </div>
                            )}
                        </CustomDialog>
                        <div className="pb-5 lg:mx-auto lg:max-w-7xl lg:px-8">
                            <div className="relative mt-8">
                                <div className="relative -mb-6 w-full overflow-x-auto pb-6">
                                    <ul
                                        role="list"
                                        className="mx-4 inline-flex space-x-8 sm:mx-6 lg:mx-0 lg:grid lg:grid-cols-4 lg:gap-x-8 lg:space-x-0"
                                    >
                                        {clothing && clothing.map((clothing, index) => (
                                            <div key={index} className="inline-flex w-64 flex-col text-center lg:w-auto">
                                                <div className="group relative">
                                                    <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200">
                                                        <img
                                                            onClick={() => handleOpen(clothing)}
                                                            src={clothing.Image}
                                                            alt='image'
                                                            className="h-full w-full object-cover object-center group-hover:opacity-75"
                                                        />
                                                    </div>
                                                </div>
                                                <ul role="list" className="mt-auto flex items-center justify-center space-x-3 py-6">
                                                    <li
                                                        className="h-4 w-4 rounded-full border border-black border-opacity-10"
                                                        style={{ backgroundColor: colors.find((item) => Number(item.index) == clothing.Color)?.description || 'transparent' }}
                                                    >
                                                    </li>
                                                </ul>
                                            </div>
                                        ))}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }
        </>
    );
};

export default HomePage;
