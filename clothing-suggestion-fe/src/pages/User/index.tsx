import React, { useEffect, useState } from 'react';
import CustomDialog from '../../components/Dialog';
import { IUser } from '../../types';
import { createUser, deleteUser, getAllUsers, updateUser } from '../../hooks/user.hook';
import { useSelector } from 'react-redux';
import ConfirmationDialog from '../../components/ConfirmationDialog';
import Toast from '../../components/Toast';

const UserPage = () => {
    const [open, setOpen] = useState(false);
    const [openConfirmDelete, setOpenConfirmDelete] = useState(false);
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [isLoad, setIsLoad] = useState(true);
    const [users, setUsers] = useState<IUser[]>([]);
    const valueSearch = useSelector((state: any) => state.valueSearch);
    const [deleteUserId, setDeleteUserId] = useState<string>('');
    const [formData, setFormData] = useState({
        id: '',
        email: '',
        username: '',
        avatar: '',
        role: 'admin',
    });
    useEffect(() => {
        async function fetchUser() {
            try {
                const response = await getAllUsers(valueSearch.value as string);
                setUsers(response.data.items);
            } catch (error) {
                console.error("Error fetching users:", error);
            }
        }
        fetchUser();
    }, [isLoad, valueSearch]);
    const handleChange = (e: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLSelectElement>) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };
    

    const handleOpenEditModal = (item: IUser) => {
        setFormData(item);
        setIsEditModalOpen(true);
    };
    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            if (formData.id) {
                await updateUser(formData.id, formData);
            } else {
                await createUser({
                    email: formData.email,
                    username: formData.username,
                    password: '123456aA@',
                    avatar: formData.avatar,
                    role: formData.role,
                });
            }
            handleClose();
            setIsLoad(!isLoad);
            handleShowToast("Lưu thành công", "success")
        } catch (error) {
            console.error("Error creating user:", error);
            handleShowToast("Lưu thất bại", "danger")
        }
    };
    const handleClose = () => {
        setOpen(false);
        setIsEditModalOpen(false);
        setFormData({
            id: '',
            email: '',
            username: '',
            avatar: '',
            role: '',
        });
    };

    const handleRemove = async (id: string) => {
        setDeleteUserId(id);
        setOpenConfirmDelete(true);
    };

    const handleConfirmDelete = async () => {
        try {
            await deleteUser(deleteUserId);
            setIsLoad(!isLoad);
        } catch (error) {
            console.error("Error deleting user:", error);
            handleShowToast('Xóa thất bại', "danger")
        } finally {
            setOpenConfirmDelete(false);
            handleShowToast("Xóa thành công", "success")
        }
    };

    const handleCloseDeleteConfirm = () => {
        setOpenConfirmDelete(false);
    };

    const [showToast, setShowToast] = useState(false);
    const [message, setMessage] = useState('');
    const [type, setType] = useState('');

    const handleShowToast = (msg: any, t: any) => {
        setMessage(msg);
        setType(t);
        setShowToast(true);
    };

    return (
        <div>
            <Toast message={message} type={type} showToast={showToast} setShowToast={setShowToast} />
            <ConfirmationDialog
                isOpen={openConfirmDelete}
                message="Are you sure you want to delete this user?"
                onConfirm={handleConfirmDelete}
                onCancel={handleCloseDeleteConfirm}
            />
            <div className="sm:flex sm:items-center">
                <div className="sm:flex-auto">
                    <h1 className="text-base font-semibold leading-6 text-gray-900">User</h1>
                </div>
                <div className="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
                    <button
                        onClick={() => setOpen(true)}
                        type="button"
                        className="rounded-md bg-white px-3.5 py-2.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-blue-100"
                    >
                        Add user
                    </button>
                </div>
                <CustomDialog isOpen={open || isEditModalOpen} onClose={handleClose}>
                    <form className="w-full flex flex-col gap-5" onSubmit={handleSubmit}>
                        <div>
                            <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-900">
                                Email
                            </label>
                            <div className="mt-2">
                                <input
                                    type="email"
                                    name="email"
                                    value={formData.email}
                                    onChange={handleChange}
                                    required
                                    className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                    placeholder="Email"
                                />
                            </div>
                        </div>
                        <div>
                            <label htmlFor="username" className="block text-sm font-medium leading-6 text-gray-900">
                                Name
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    name="username"
                                    value={formData.username}
                                    onChange={handleChange}
                                    required
                                    className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                    placeholder="Name"
                                />
                            </div>
                        </div>
                        <div>
                            <label htmlFor="avatar" className="block text-sm font-medium leading-6 text-gray-900">
                                Avatar
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    name="avatar"
                                    value={formData.avatar}
                                    onChange={handleChange}
                                    required
                                    className="w-full px-4 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2 text-gray-900 placeholder:text-gray-400 outline-none"
                                    placeholder="Avatar"
                                />
                            </div>
                        </div>
                        <div>
                            <label htmlFor="role" className="block text-sm font-medium leading-6 text-gray-900">
                                Role
                            </label>
                            <div className="mt-2">
                                <select
                                    name="role"
                                    value={formData.role}
                                    onChange={handleChange}
                                    required
                                    className="w-full pl-4 pr-5 rounded-md border-0 ring-1 ring-inset ring-gray-300 py-2.5 text-gray-900 placeholder:text-gray-400 outline-none"
                                >
                                    <option value="admin">Admin</option>
                                    <option value="user">User</option>
                                </select>
                            </div>
                        </div>

                        <div className="flex items-center justify-end py-5">
                            <button
                                type="submit"
                                className="rounded-md bg-white px-3.5 py-2.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-blue-100"
                            >
                                Save
                            </button>
                        </div>
                    </form>
                </CustomDialog>
            </div>

            <div className="mt-8 flow-root">
                <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                        <table className="min-w-full divide-y divide-gray-300">
                            <thead>
                                <tr>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Name
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Email
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Role
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Avatar
                                    </th>
                                    <th scope="col" className="py-3.5 pl-3 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-1">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="bg-white">
                                {users.map((item, index) => (
                                    <tr key={item.id} className={index % 2 === 0 ? 'even:bg-gray-50' : ''}>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.username}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.email}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">{item.role}</td>
                                        <td className="whitespace-nowrap px-1 py-4 text-sm text-gray-500">
                                            <div className="h-11 w-11 flex-shrink-0">
                                                <img className="h-11 w-11 rounded-full" src={item.avatar} alt="avatar" />
                                            </div>
                                        </td>
                                        <td className="relative whitespace-nowrap py-4 pl-1 pr-4 text-sm font-medium sm:pr-3">
                                            <button
                                                onClick={() => handleOpenEditModal(item)}
                                                type="button"
                                                className="mr-2 inline-flex items-center rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 disabled:cursor-not-allowed disabled:opacity-30 disabled:hover:bg-white"
                                            >
                                                Edit
                                            </button>
                                            <button
                                                onClick={() => handleRemove(item.id)}
                                                type="button"
                                                className="inline-flex items-center rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50 disabled:cursor-not-allowed disabled:opacity-30 disabled:hover:bg-white"
                                            >
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default UserPage;