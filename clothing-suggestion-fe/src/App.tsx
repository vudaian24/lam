import React from 'react';
import AppRouter from './routes';
import { PrimeReactProvider } from 'primereact/api';

function App() {
  return (
    <>
      <PrimeReactProvider>
        <AppRouter />
      </PrimeReactProvider>
    </>
  );
}

export default App;
