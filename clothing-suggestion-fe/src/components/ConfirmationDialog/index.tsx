import React from 'react';
interface Props {
    isOpen: boolean;
    message: string;
    onConfirm: () => void;
    onCancel: () => void;
  }

const ConfirmationDialog = ({ isOpen, message, onConfirm, onCancel }: Props) => {
    return (
        <div className={`fixed z-50 inset-0 overflow-y-auto ${isOpen ? '' : 'hidden'}`}>
            <div className="flex items-center justify-center min-h-screen p-4 text-center">
                <div className="fixed inset-0 bg-gray-500 opacity-75"></div>
                <div className="relative bg-white w-1/3 rounded-lg p-6">
                    <div className="text-lg mb-4">{message}</div>
                    <div className="flex justify-center">
                        <button
                            className="px-4 py-2 bg-red-500 text-white rounded hover:bg-red-600 mr-4"
                            onClick={onConfirm}
                        >
                            Confirm
                        </button>
                        <button
                            className="px-4 py-2 bg-gray-400 text-white rounded hover:bg-gray-500"
                            onClick={onCancel}
                        >
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ConfirmationDialog;
