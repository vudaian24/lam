import { Fragment, useRef } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { XMarkIcon } from '@heroicons/react/24/outline';

interface CustomDialogProps {
    children: React.ReactNode;
    isOpen: boolean;
    onClose: () => void;
}

const CustomDialog: React.FC<CustomDialogProps> = ({ isOpen, onClose, children }: CustomDialogProps) => {
    const cancelButtonRef = useRef<HTMLButtonElement>(null);

    return (
        <Transition.Root show={isOpen} as={Fragment}>
            <Dialog as="div" className="relative z-40" initialFocus={cancelButtonRef} onClose={onClose}>
                <Transition.Child
                    as={Fragment}
                    enter="ease-out duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                </Transition.Child>

                <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
                    <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                        <Transition.Child
                            as={Fragment}
                            enter="ease-out duration-300"
                            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                            enterTo="opacity-100 translate-y-0 sm:scale-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        >
                            <Dialog.Panel className="relative transform border-[1px] border-solid border-gray-300 overflow-hidden rounded-2xl bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-2xl">
                                <div className="pr-4 py-4 border-b-gray-300 bg-gray-200 border-[1px] border-solid flex items-center justify-end">
                                    <button
                                        type="button"
                                        className="rounded-md bg-gray-200 text-gray-400 hover:text-gray-500 outline-none"
                                        onClick={onClose}
                                    >
                                        <XMarkIcon className="h-6 w-6 outline-none" aria-hidden="true" />
                                    </button>
                                </div>
                                <div className='flex p-10 w-full'>
                                    {children}
                                </div>
                            </Dialog.Panel>
                        </Transition.Child>
                    </div>
                </div>
            </Dialog>
        </Transition.Root>
    );
};

export default CustomDialog;
