import React from 'react';

const Loading = () => {
    return (
        <div className="flex justify-center items-center h-[700px]">
            <div className='w-full h-full flex items-center justify-center z-50 bg-white opacity-70'>
                <div
                    className="inline-block h-10 w-10 animate-spin rounded-full border-[5px] border-solid border-current border-e-white border-e-[6px]
                 text-surface motion-reduce:animate-[spin_1.5s_linear_infinite] text-black"
                    role="status">
                </div>
            </div>

        </div>
    );
};

export default Loading;
