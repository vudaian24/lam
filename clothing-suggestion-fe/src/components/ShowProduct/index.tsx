import React, { useState } from 'react';
import { ItemSuccess } from '../../types';
import CustomDialog from '../Dialog';

interface Props {
    data: ItemSuccess[];
}

const ShowProduct = ({
    data
}: Props) => {
    const [isOpen, setIsOpen] = useState(false);
    const [selectItem, setSelectItem] = useState<any>(null);

    const handleOpen = async (item: any) => {
        setIsOpen(true);
        setSelectItem(item);
    }
    const handleClose = () => {
        setIsOpen(false);
    }

    return (
        <ul role="list" className="grid grid-cols-6 gap-x-4 gap-y-8 sm:gap-x-6 xl:gap-x-8">
            <CustomDialog isOpen={isOpen} onClose={handleClose}>
                {selectItem && (
                    <div>
                        <img src={selectItem?.Image} alt="" />
                    </div>
                )}
            </CustomDialog>
            {data.map((file, index) => (
                <li key={index} className="relative" onClick={() => handleOpen(file)}>
                    <div className="group aspect-h-7 aspect-w-10 block w-full overflow-hidden rounded-lg bg-gray-100">
                        <img src={file.Image} alt="" className="pointer-events-none object-cover group-hover:opacity-75" />
                    </div>
                </li>
            ))}
        </ul>
    );
};

export default ShowProduct;