import React, { useEffect } from 'react';

interface Props {
    message: string;
    type: string;
    showToast: boolean;
    setShowToast: React.Dispatch<React.SetStateAction<boolean>>;
}

const Toast = ({ message, type, showToast, setShowToast }: Props) => {
    useEffect(() => {
        if (showToast) {
            const timer = setTimeout(() => {
                setShowToast(false);
            }, 3000);

            return () => clearTimeout(timer);
        }
    }, [showToast, setShowToast]);

    return (
        <>
            {showToast && (
                <div className={`fixed h-[60px] w-[250px] right-3 top-3 flex items-center justify-center z-50`}>
                    <div className={`${type === 'danger' ? 'bg-red-400' : 'bg-green-400'} text-white py-2 px-4 rounded-md shadow-lg w-full`}>
                        {message}
                    </div>
                </div>
            )}
        </>
    );
};

export default Toast;
