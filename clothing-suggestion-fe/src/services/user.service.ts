import axiosInstance, { ApiService } from "../plugins/axios";
import { IBodyResponse, IGetListResponse } from "../common/interfaces";
import { createUserProps, updateUserProps } from "../types";

class UserApiService extends ApiService {
    constructor() {
        super({
            baseUrl: '/user'
        }, axiosInstance);
    }

    getAll<T>(value: string): Promise<IBodyResponse<IGetListResponse<T>>> {
        return this.client.get(`${this.baseUrl}`, {
            params: {username: value}
        });
    }

    create(body: createUserProps): Promise<IBodyResponse<any>> {
        return this.client.post(`${this.baseUrl}`, body);
    }

    getDetail<R>(id: string | number): Promise<R> {
        return this._getDetail<R>(id);
    }

    update(body: updateUserProps): Promise<IBodyResponse<any>> {
        return this._update(body.id, body.body);
    }

    delete(id: string): Promise<IBodyResponse<any>> {
        return this._delete<any>(id);
    }
}

export const userApi = new UserApiService();
