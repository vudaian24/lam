import axiosInstance, { ApiService } from "../plugins/axios";
import { IBodyResponse, IGetListResponse } from "../common/interfaces";
import { createCategoryProps, updateCategoryProps } from "../types";

class CategoryApiService extends ApiService {
    constructor() {
        super({
            baseUrl: '/category'
        }, axiosInstance);
    }

    getAll<T>(value: string): Promise<IBodyResponse<IGetListResponse<T>>> {
        return this.client.get(`${this.baseUrl}`, {
            params: { name: value }
        });
    }

    create(body: createCategoryProps): Promise<IBodyResponse<any>> {
        return this.client.post(`${this.baseUrl}`, body);
    }

    getDetail<R>(id: string | number): Promise<R> {
        return this._getDetail<R>(id);
    }

    update(body: updateCategoryProps): Promise<IBodyResponse<any>> {
        return this._update(body.id, body.body);
    }

    delete(id: string): Promise<IBodyResponse<any>> {
        return this._delete<any>(id);
    }
}

export const categoryApi = new CategoryApiService();
