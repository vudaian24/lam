import axiosInstance, { ApiService } from "../plugins/axios";
import { IBodyResponse, IGetListResponse } from "../common/interfaces";
import { createClothingProps, updateClothingProps } from "../types";
import axios from "axios";

class ClothingApiService extends ApiService {
    constructor() {
        super({
            baseUrl: '/product'
        }, axiosInstance);
    }

    getAll<T>(value: string): Promise<IBodyResponse<IGetListResponse<T>>> {
        return this._getList<T>({
            name: value
        });
    }

    getProductByCategory(body: number): Promise<IBodyResponse<any>> {
        // return this.client.post(`${this.baseUrl}/category`, {
        //     query: body
        // });
        return axios.post(`http://127.0.0.1:8000/all/`, {
            Category: body
        });
    }

    create(body: createClothingProps): Promise<IBodyResponse<any>> {
        return this._create(body);
    }

    getDetail<R>(id: string | number): Promise<R> {
        return this._getDetail<R>(id);
    }

    update(body: updateClothingProps): Promise<IBodyResponse<any>> {
        return this._update(body.id, body.body);
    }

    delete(id: string): Promise<IBodyResponse<any>> {
        return this._delete<any>(id);
    }
}

export const clothingApi = new ClothingApiService();
