import axiosInstance, { ApiService } from "../plugins/axios";
import { IBodyResponse, IGetListResponse } from "../common/interfaces";
import { createBrandProps, updateBrandProps } from "../types";

class BrandApiService extends ApiService {
    constructor() {
        super({
            baseUrl: '/brand'
        }, axiosInstance);
    }

    getAll<T>(value: string): Promise<IBodyResponse<IGetListResponse<T>>> {
        return this.client.get(`${this.baseUrl}`, {
            params: { name: value }
        });
    }
    
    getAllStyles<T>(): Promise<IBodyResponse<IGetListResponse<T>>> {
        return this.client.get(`${this.baseUrl}/all`);
    }

    create(body: createBrandProps): Promise<IBodyResponse<any>> {
        return this.client.post(`${this.baseUrl}`, body);
    }

    getDetail<R>(id: string | number): Promise<R> {
        return this._getDetail<R>(id);
    }

    update(body: updateBrandProps): Promise<IBodyResponse<any>> {
        return this._update(body.id, body.body);
    }

    delete(id: string): Promise<IBodyResponse<any>> {
        return this._delete<any>(id);
    }
}

export const brandApi = new BrandApiService();
