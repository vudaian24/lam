import axiosInstance, { ApiService } from "../plugins/axios";
import { IBodyResponse, IGetListResponse } from "../common/interfaces";
import { createColorProps, updateColorProps } from "../types";

class ColorApiService extends ApiService {
    constructor() {
        super({
            baseUrl: '/color'
        }, axiosInstance);
    }

    getAll<T>(value: string): Promise<IBodyResponse<IGetListResponse<T>>> {
        return this.client.get(`${this.baseUrl}`, {
            params: { name: value }
        });
    }

    create(body: createColorProps): Promise<IBodyResponse<any>> {
        return this.client.post(`${this.baseUrl}`, body);
    }

    getDetail<R>(id: string | number): Promise<R> {
        return this._getDetail<R>(id);
    }

    update(body: updateColorProps): Promise<IBodyResponse<any>> {
        return this._update(body.id, body.body);
    }

    delete(id: string): Promise<IBodyResponse<any>> {
        return this._delete<any>(id);
    }
}

export const colorApi = new ColorApiService();