import { userApi } from "../services/user.service";

export async function getAllUsers(value: string): Promise<any> {
    try {
        return await userApi.getAll(value);
    } catch (error) {
        console.error("Error fetching all users:", error);
        throw error;
    }
}

export async function getUserDetail(id: string): Promise<any> {
    try {
        return await userApi.getDetail(id);
    } catch (error) {
        console.error("Error fetching user detail:", error);
        throw error;
    }
}

export async function createUser(data: any): Promise<any> {
    try {
        return await userApi.create(data);
    } catch (error) {
        console.error("Error creating user:", error);
        throw error;
    }
}

export async function updateUser(id: string, data: any): Promise<any> {
    try {
        return await userApi.update({ id: id, body: data });
    } catch (error) {
        console.error("Error updating user:", error);
        throw error;
    }
}

export async function deleteUser(id: string): Promise<any> {
    try {
        return await userApi.delete(id);
    } catch (error) {
        console.error("Error deleting user:", error);
        throw error;
    }
}
