import { brandApi } from "../services/brand.service";

export async function getAllBrands(value: string): Promise<any> {
    try {
        return await brandApi.getAll(value);
    } catch (error) {
        console.error("Error fetching all brands:", error);
        throw error;
    }
}

export async function getAllStyles(): Promise<any> {
    try {
        return await brandApi.getAllStyles();
    } catch (error) {
        console.error("Error fetching all strle:", error);
        throw error;
    }
}


export async function getBrandDetail(id: string): Promise<any> {
    try {
        return await brandApi.getDetail(id);
    } catch (error) {
        console.error("Error fetching brand detail:", error);
        throw error;
    }
}

export async function createBrand(data: any): Promise<any> {
    try {
        return await brandApi.create(data);
    } catch (error) {
        console.error("Error creating brand:", error);
        throw error;
    }
}

export async function updateBrand(id: string, data: any): Promise<any> {
    try {
        return await brandApi.update({ id: id, body: data });
    } catch (error) {
        console.error("Error updating brand:", error);
        throw error;
    }
}

export async function deleteBrand(id: string): Promise<any> {
    try {
        return await brandApi.delete(id);
    } catch (error) {
        console.error("Error deleting brand:", error);
        throw error;
    }
}
