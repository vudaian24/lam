import { categoryApi } from "../services/category.service";

export async function getAllCategories(value: string): Promise<any> {
    try {
        return await categoryApi.getAll(value);
    } catch (error) {
        console.error("Error fetching all categories:", error);
        throw error;
    }
}

export async function getCategoryDetail(id: string): Promise<any> {
    try {
        return await categoryApi.getDetail(id);
    } catch (error) {
        console.error("Error fetching category detail:", error);
        throw error;
    }
}

export async function createCategory(data: any): Promise<any> {
    try {
        return await categoryApi.create(data);
    } catch (error) {
        console.error("Error creating category:", error);
        throw error;
    }
}

export async function updateCategory(id: string, data: any): Promise<any> {
    try {
        return await categoryApi.update({ id: id, body: data });
    } catch (error) {
        console.error("Error updating category:", error);
        throw error;
    }
}

export async function deleteCategory(id: string): Promise<any> {
    try {
        return await categoryApi.delete(id);
    } catch (error) {
        console.error("Error deleting category:", error);
        throw error;
    }
}
