import { sizeApi } from "../services/size.service";

export async function getAllSizes(value: string): Promise<any> {
    try {
        return await sizeApi.getAll(value);
    } catch (error) {
        console.error("Error fetching all sizes:", error);
        throw error;
    }
}

export async function getSizeDetail(id: string): Promise<any> {
    try {
        return await sizeApi.getDetail(id);
    } catch (error) {
        console.error("Error fetching size detail:", error);
        throw error;
    }
}

export async function createSize(data: any): Promise<any> {
    try {
        return await sizeApi.create(data);
    } catch (error) {
        console.error("Error creating size:", error);
        throw error;
    }
}

export async function updateSize(id: string, data: any): Promise<any> {
    try {
        return await sizeApi.update({ id: id, body: data });
    } catch (error) {
        console.error("Error updating size:", error);
        throw error;
    }
}

export async function deleteSize(id: string): Promise<any> {
    try {
        return await sizeApi.delete(id);
    } catch (error) {
        console.error("Error deleting size:", error);
        throw error;
    }
}
