import { clothingApi } from "../services/clothing.service";

export async function getAllClothings(value: string): Promise<any> {
    try {
        return await clothingApi.getAll(value);
    } catch (error) {
        console.error("Error fetching all clothing:", error);
        throw error;
    }
}

export async function getProductByCategory(value: number): Promise<any> {
    try {
        return await clothingApi.getProductByCategory(value);
    } catch (error) {
        console.error("Error fetching all clothing:", error);
        throw error;
    }
}



export async function getClothingDetail(Id: string): Promise<any> {
    try {
        return await clothingApi.getDetail(Id);
    } catch (error) {
        console.error("Error fetching clothing detail:", error);
        throw error;
    }
}

export async function createClothing(data: any): Promise<any> {
    try {
        return await clothingApi.create(data);
    } catch (error) {
        console.error("Error creating clothing:", error);
        throw error;
    }
}

export async function updateClothing(id: string, data: any): Promise<any> {
    try {
        return await clothingApi.update({ id: id, body: data });
    } catch (error) {
        console.error("Error updating clothing:", error);
        throw error;
    }
}

export async function deleteClothing(id: string): Promise<any> {
    try {
        return await clothingApi.delete(id);
    } catch (error) {
        console.error("Error deleting clothing:", error);
        throw error;
    }
}


