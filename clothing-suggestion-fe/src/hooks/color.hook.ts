import { colorApi } from "../services/color.service";

export async function getAllColors(value: string): Promise<any> {
    try {
        return await colorApi.getAll(value);
    } catch (error) {
        console.error("Error fetching all colors:", error);
        throw error;
    }
}

export async function getColorDetail(id: string): Promise<any> {
    try {
        return await colorApi.getDetail(id);
    } catch (error) {
        console.error("Error fetching color detail:", error);
        throw error;
    }
}

export async function createColor(data: any): Promise<any> {
    try {
        return await colorApi.create(data);
    } catch (error) {
        console.error("Error creating color:", error);
        throw error;
    }
}

export async function updateColor(id: string, data: any): Promise<any> {
    try {
        return await colorApi.update({ id: id, body: data });
    } catch (error) {
        console.error("Error updating color:", error);
        throw error;
    }
}

export async function deleteColor(id: string): Promise<any> {
    try {
        return await colorApi.delete(id);
    } catch (error) {
        console.error("Error deleting color:", error);
        throw error;
    }
}
